Tutorial: Part 2 - PyTest Integration
=====================================

This part of the tutorial goes over the ``pytest`` integration of ``pyedmgr``.
This deserves its own section as its internals are fairly complicated, although
the interface is (hopefully) quite simple.

Test Cases
----------

The central component from the user's perspective is ``fixture_test_case``,
which is a ``pytest`` fixture that instantiates all the machinery we covered in
the first part of the tutorial based on what should usually be a fairly simple
test case descriptor from the user, then cleans it up at the end of the test.

Here is an example showing what the descriptor might look like:

.. code:: python

    @pytest.mark.asyncio
    @pytest.mark.parametrize(
        'fixture_test_case',
        [
            # <test-case descriptor>
            {
                'FVP_Corstone_SSE-300_Ethos-U55': {
                    'firmware': ['firmware.elf']
                }
            }
            # </test-case descriptor>
        ],
        indirect=True
    )
    async def test_example_test(fixture_test_case:TestCase):
        # ...

This tells ``pyedmgr`` to create an instance of
``FVP_Corstone_SSE-300_Ethos-U55`` and flash ``firmware.elf`` to it when the
test starts.

``fixture_test_case`` doesn't do this work itself, it offloads it to an object
graph which starts with a factory method, ``TestCase.parse``. This creates an
instance of ``TestCaseParser``, which reads the test case descriptor and turns
it into a ``TestCase``.

Each ``TestCase`` receives a list of pairs of device (FVP- or board-) names and
the configuration for one instance of the device.

``TestCase`` is an asynchronous context manager. When the context is entered,
it instantiates a ``Host`` and ``TestCaseContext``. The ``TestCaseContext``
continues the setup process by connecting to the appropriate devices and
flashing the firmware to them.

The test can then access the flashed devices using the
``TestCaseContext.allocated_devices`` property. As touched on in the previous
tutorial, the ``pytest`` integration package uses ``TestDevice`` instances
instead of the usual ``Device`` instance. As before there is not much practical
difference and you can treat ``TestDevice`` as a ``Device`` with an extra

More on Test Case Descriptors
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The parsing of a test-case descriptor is quite flexible and allows for some
useful tricks:

Create multiple of the same device
''''''''''''''''''''''''''''''''''

You can create multiple instances of the same board or FVP by nesting the
configuration dictionary inside another dictionary.

.. code:: python

    {
        'FVP_Corstone_SSE-300_Ethos-U55': {
            1: {'firmware': ['firmware.elf']},
            2: {'firmware': ['firmware.elf']},
        }
    }

The names of the keys don't matter, as long as they are unique.

FVP config file and command-line arguments
''''''''''''''''''''''''''''''''''''''''''

You can override the default configuration file and command-line arguments for
FVPs:

.. code:: python

    {
        'FVP_Corstone_SSE-300_Ethos-U55': {
            'firmware': ['firmware.elf'],
            'config': 'MPS3.conf',
            'args': ['--quantum', '10']
        }
    }

Simplify specification
''''''''''''''''''''''

When a device is only passed a firmware list you can remove the inner dictionary
altogether:

.. code:: python

    {
        'FVP_Corstone_SSE-300_Ethos-U55': ['firmware.elf']
    }

Which can be mixed-and-matched:

.. code:: python

    {
        'FVP_Corstone_SSE-300_Ethos-U55': {
            1: {'firmware': ['firmware.elf'], 'config': 'MPS3.conf'},
            2: ['firmware.elf'],
        }
    }

Load specification from JSON
''''''''''''''''''''''''''''

And finally you can also load the entire test case from a file:

.. code:: python

    @pytest.mark.asyncio
    @pytest.mark.parametrize(
        'fixture_test_case',
        [
            "@test_case.json"
        ],
        indirect=True
    )
    # ...

The syntax is identical whether you use a Python dictionary or JSON file,
including all of the above scenarios.

Logging
-------

By default, when ``pyedmgr.test`` is imported it will set up logging with
``coloredlogs.install`` or ``logging.basicConfig`` depending on whether
``stdout`` is attached to a terminal or file/pipe.

This can be prevented by setting ``pyedmgr.test.flags.SETUP_LOGGING`` to false
before importing ``pyedmgr``.

There is also a ``logger`` fixture which is provided for convenience.
