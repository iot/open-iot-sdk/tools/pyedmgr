.. pyedmgr documentation master file, created by
   sphinx-quickstart on Tue Apr  4 16:21:40 2023.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to pyedmgr's documentation!
===================================

Introduction
------------------

``pyedmgr`` (**Py**\ thon **E**\ mbedded **D**\ evice **M**\ ana\ **g**\ e\ **r**; pronounced "pied (as in Pied Piper) manager")
is a Python library for programmatic
discovery, flashing and communication with Arm Virtual Hardware and mbed
OS-compatible boards.

Use-cases
~~~~~~~~~

1. ``pyedmgr`` can discover, flash and communicate with mbed-compatible boards
   and FVPs
2. ``pyedmgr`` provides ``pytest`` fixtures to make testing on virtual
   hardware and boards easy

.. toctree::
   :maxdepth: 2
   :caption: Contents:


Guides
------------------

* `Quick Start Guide <./quickstart.html>`__
* Tutorial

  * `Part 1 - Concepts & Components <./tutorial1.html>`__
  * `Part 2 - PyTest Integration <./tutorial2.html>`__

API Reference
------------------

* :ref:`modindex`
* :ref:`search`
