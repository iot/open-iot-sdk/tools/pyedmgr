#!/bin/bash

# Copyright (c) 2022, Arm Limited and Contributors. All rights reserved.
# SPDX-License-Identifier: Apache-2.0

HERE="$(realpath "$(dirname "${BASH_SOURCE[0]}")")"

cd "$HERE" || exit 1

mkdir -p _static
mkdir -p _templates
mkdir -p _build

# Re-generate the rsts
sphinx-apidoc -o . ../pyedmgr

# Generate HTML
PYTHONPATH=.. sphinx-build . _build ./*.rst
