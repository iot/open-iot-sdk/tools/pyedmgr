Quick Start Guide
=================

This quick start guide explains how to install, configure and use ``pyedmgr``.

Installation & Required Setup
-----------------------------

System Dependencies
~~~~~~~~~~~~~~~~~~~

To use Arm Virtual Hardware (AVH) ``pyedmgr`` you must have an Arm
Virtual Hardware product such as FVP, and the ``Iris`` Python library
installed.

Python Dependencies
~~~~~~~~~~~~~~~~~~~

You can install ``pyedmgr`` itself and its Python dependencies to your
environment or virtual environment using ``setuptools``.

.. code:: sh

   python setup.py build
   python setup.py install

This will handle installing the requirements automatically.

To install the dependencies only, use ``pip``:

.. code:: sh

   pip install -r requirements.txt

Environment setup
~~~~~~~~~~~~~~~~~

Arm Virtual Hardware
^^^^^^^^^^^^^^^^^^^^

To use Arm Virtual Hardware, ``pyedmgr`` needs to know where the model
binary and ``Iris`` library are.

Automatic discovery
'''''''''''''''''''

For a limited set of products, placing the executable’s location in
``PATH`` is sufficient for ``pyedmgr`` to find ``Iris``. The products
are:

-  ``FVP_Corstone_SSE-300_Ethos-U55`` and ``U65``
-  ``FVP_Corstone_SSE-310_Ethos-U55`` and ``U65``
-  ``VHT_Corstone_SSE-300_Ethos-U55`` and ``U65``
-  ``VHT_Corstone_SSE-310_Ethos-U55`` and ``U65``

If the binaries are not in PATH or there are multiple different
versions, you can choose a specific location by setting an environment
variable:

.. code:: sh

   export FVP_CS300_U65_INSTALL_PATH=/path/to/fvp_directory
   export VHT_CS310_U55_INSTALL_PATH=/path/to/fvp_directory

Iris is included with these products and will be found automatically
from one of the locations.

Other Situation
'''''''''''''''

Where your FVP product is not found automatically, or you want to use a
different Iris version than the one bundled with a product, set
``IRIS_INSTALL_DIR`` to the location of the Iris module, e.g.

.. code:: sh

   export IRIS_INSTALL_DIR=/opt/VHT_Corstone_SSE-300/Iris/Python

or include it in ``PYTHONPATH``:

.. code:: sh

   export PYTHONPATH=$PYTHONPATH:/opt/VHT_Corstone_SSE-300/Iris/Python

Runtime FVP Configuration
'''''''''''''''''''''''''

You may add, remove or change FVP parameters by placing a file called
``MPS3.conf`` in the current working directory. ``pyedmgr`` will use
this file by default as a configuration file for the FVP.

For a list of available parameters, run the FVP with the
``--list-params`` flag.

Boards
^^^^^^

To use mbed/DAPlink compatible boards, you will need a board flashed
with appropriate firmware, a USB cable, and possibly, if using Linux, a
set of ``udev`` rules.

Follow `these
instructions <https://github.com/pyocd/pyOCD/blob/main/udev/README.md>`__
to set up the ``udev`` rules.

Once this is done connect the board via the cable.
If you will be flashing firmware to the board, mount its filesystem somewhere
to make it flashable. If you will be running ``pyedmgr`` as a normal
user, mount with ``-o umask=000`` or make the mount point writeable as
root.

Code Examples
-------------

After installation & setup you should be able to ``import pyedmgr`` and
start using it.

Discovering Boards
~~~~~~~~~~~~~~~~~~~

This example shows how to use ``pyedmgr`` to discover, flash and communicate
with boards. The example can work with one or several boards and they can be the
same or different types.

As we will be flashing the boards, they need to be connected in firmware
flashing mode, which is achieved by holding down the RESET button while
connecting the board via USB. Once the boards are flashed the example will pause
and wait for you to reconnect the boards so that they boot the firmware.

Once this is done the example echoes the output of the boards to the console.

.. code:: python

   import asyncio
   import logging
   import sys
   from typing import List

   # Import the classes we need from pyedmgr
   from pyedmgr import DeviceFactory, Host, RealDeviceRegistry

   async def main(firmwares:List[str]):
      # Create device Host and its dependencies. The Host is responsible for
      # managing device connections.
      # These lines can also be replaced by: host = Host.default()
      logging.info('Creating device host')
      factory  = DeviceFactory()
      registry = RealDeviceRegistry(factory)
      host     = Host(registry)

      # establish_connections() tells the Host to discover boards (via the Registry)
      # and create serial connections to them.
      logging.info('Discovering devices')
      connected_devices = await host.establish_connections()
      if not connected_devices:
         logging.error('No devices found')
         exit(1)
      logging.info(f'{len(connected_devices)} devices connected')

      # Flash firmware to the devices.
      flashed_devices, unflashed_devices = await host.flash_connected_devices(firmware)
      if flashed_devices:
         logging.info(f'Flashed {len(flashed_devices)} devices')
      else:
         logging.error('No devices flashed')
         exit(1)

      # Disconnect unflashed devices. This generally happens if there is no firmware
      # that matches a device.
      if unflashed_devices:
         logging.warning(
            'Could not flash devices: ' + ', '.join(
               map(
                  lambda d: d.name,
                  unflashed_devices
               )
            )
         )

      # Re-connect
      await host.disconnect_all_devices()
      input('Please re-connect boards and press ENTER')
      connected_devices = await host.establish_connections()

      # Echo output from boards
      async def callback(device:Device, bs:bytes):
         s = bs.decode('utf-8')
         logging.info(f'{device.name}: {s}')

      try:
         await host.communicate(callback)
      except KeyboardInterrupt:
         logging.info('Stopping')
         await host.disconnect_all_devices()

   if __name__ == '__main__':
      if len(sys.argv) < 2:
         print(f'Usage: {sys.argv[0]} <firmware> [more firmware]', file=sys.stderr)
         exit(1)

      logging.basicConfig()
      asyncio.run(main(sys.argv[1:]))

Integration with Pytest
~~~~~~~~~~~~~~~~~~~~~~~

This example shows how to use ``pyedmgr`` with ``pytest`` to run tests
against a firmware binary. As creating a binary is outside of the scope
of this guide, we will use the ``cs300-u55.elf`` binary that comes with
the ``pyedmgr`` tests. When executed on a Corstone 300 Ethos-U55 FVP
this binary simply writes “hello, world” to the serial console.

.. code:: python

   import pytest

   # Import pytest fixture which creates a test case
   from pyedmgr import fixture_test_case

   # Parameterise the fixture
   @pytest.mark.asyncio
   @pytest.mark.parametrize(
       'fixture_test_case',
       [
           # One set of parameters. This can be repeated to create multiple runs of the test with different
           # settings.
           {
               # Look for and launch FVP_Corstone_SSE-300_Ethos-U55. It will raise KeyError if there is neither a board nor executable in PATH with that name.
               'FVP_Corstone_SSE-300_Ethos-U55': {
                   # Flash the following firmware. The {{ }} syntax means replace the string with an environment variable.
                   'firmware': ['{{PWD}}/cs300-u55.elf'],
                   # Use the following configuration file. The default is to look for MPS3.conf in the working directory.
                   'config': 'my-mps3-config.conf'
               }
           },
       ],
       # Required so fixture_test_case is called with the parameter values.
       indirect='fixture_test_case'
   )
   async def test__example_test(fixture_test_case):
       # Enter the test case context - this spawns and flashes the binary to the FVP
       async with fixture_test_case as context:
           # context.allocted_devices[0] now refers to a `TestDevice` instance which we can use to talk to the firmware
           # TestDevice.channel is an instance of `SocketChannel` which lets us read and write to the simulated serial port over a local socket
           line:bytes = context.allocated_devices[0].channel.readline_async()
           assert b'hello, world' in line
       # When the context is exited, the allocated resources are released.

In the example, ``test__example_test`` uses the ``fixture_test_case``
pytest fixture and ``parametrize`` decorator to pass a test-case
specification to ``fixture_test_case``, which returns a ``TestCase``
that pytest binds to the ``fixture_test_case`` parameter. ``TestCase``
is a class which encapsulates the parsed test case configuration,
i.e. what device(s) and firmware(s) the test needs.

In the function body, we enter the ``TestCase`` `asynchronous context
manager <https://peps.python.org/pep-0492/#asynchronous-context-managers-and-async-with>`__.
In its ``__aenter__`` method, ``TestCase`` creates and returns a
``TestCaseContext``. ``TestCaseContext`` provides access to the
connected and flashed devices via the ``Host`` object.
