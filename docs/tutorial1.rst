Tutorial - Part 1: Concepts & Components
=========================================

This tutorial provides a deep dive into ``pyedmgr``. It's recommended to read and
follow the `Quick Start Guide <./QuickStart.html>`__ first for instructions on
how to install and set up ``pyedmgr``.

The first part of the tutorial goes over the main components of ``pyedmgr``: how
they work and fit together.

They are:

* :ref:`Channels`
* :ref:`Controllers`
* :ref:`Devices` and :ref:`Device Factory`
* :ref:`Firmware` and :ref:`Firmware List`
* :ref:`Flashers`
* :ref:`Device Registry`, and
* :ref:`Host`


Channels
--------

Channels are an abstracted way to communicate with a device, providing a common
interface to devices that communicate over media such as serial or network.

Channels implement the ``AbstractChannel`` base class. They are required to
support all of its methods.

There are currently three types of channel:

* ``SerialChannel`` - communicate with devices over serial (UART) e.g. boards
* ``SocketChannel`` - communicate with devices over network e.g. FVPs
* ``StreamChannel`` - communicate with devices through streams which may be
  backed by e.g. device files or sockets.

Channels provide both synchronous and asynchronous communication with the device

Controllers
-----------

Controllers provide an interface to control the device using on-chip debugging
(compatible boards) or Iris (FVPs). This may allow functions such as resetting
or shutting down the device.

Controllers implement the ``AbstractController`` base class. For any method not
supported, a controller will return ``NotImplemented``.

There are currently two main controllers used in ``pyedmgr``:

* ``FvpController`` - launch, pause, resume, shutdown and reset an FVP. Also
  provides factory methods for the channel and flasher.
* ``MbedController`` - pause, resume and reset an mbed board

There are two additional controllers for testing with fake devices:

* ``SerialServerController`` - imitates a device running a serial echo server
* ``SocketServerController`` - imitates a device running a TCP echo server


Devices
-------

Devices represent a board, FVP or fake device and provide access to its channel
and controller as well as some higher-level functionality such as connecting and
disconnecting the channel, flashing firmware, and resetting the device. A Device
instance knows what firmware is compatible with the underlying board or FVP and
how to flash it.

Devices implement the ``AbstractDevice`` base class.

Currently there are two implementations:

* ``Device`` - the main implementation
* ``TestDevice`` - a wrapper around ``Device`` used in the ``pytest``
  integration. The main difference of concern to users is the ``communicate``
  method which automates testing a "conversation" between the test and the
  device

Device Factory
~~~~~~~~~~~~~~

Calling the ``Device`` constructor requires some information about the
underlying device. The ``DeviceFactory`` provides a simpler way to create
instances of ``Device``.

The device factory can create:

* Fake serial devices via ``DeviceFactory.fake_serial``, which use the
  ``SerialServerController``
* Fake socket devices via ``DeviceFactory.fake_socket``, which use the
  ``SocketServerController``
* FVP devices via ``DeviceFactory.load_fvp``, ``DeviceFactory.fvp_cs300_u55``
  and ``DeviceFactory.fvp_cs300_u65``
* Mbed boards via ``DeviceFactory.load_mbed``

Firmware
--------

``Firmware`` are binaries which can be flashed to a device. Firmware has a type
which must match a type (``FirmwareType``) allowed by ``Device`` to be flashed
to it.

There are currently three ``FirmwareType``s recognised by ``pyedmgr``:

* ``BINARY`` - flat binary files
* ``ELF`` - 32-bit ELF files
* ``HEX`` - binary format used by Nordic devices

``pyedmgr`` automatically detects which type a firmware belongs to.

Firmware List
~~~~~~~~~~~~~

The ``FirmwareList`` class provides a heterogenous list of ``Firmware``s and,
for devices which support it, pairs of ``(filename, address)`` which are
additional blobs that can be loaded from a file to the specified address
(currently FVP-only).

Flashers
--------

A flasher provides the means to flash firmware to a device.  The flasher class
hierarchy is fairly complex.

Flashers implement the ``AbstractFlasher`` class, an asynchronous context
manager which returns an instance of ``AbstractFlashSession`` . A user provides
a ``FirmwareList`` to ``Device.flash()`` which instantiates an implementation of
``AbstractFlasher`` and enters the context to create an
``AbstractFlashSession`` sub-class instance. ``Device.flash()`` sets the
``FirmwareList`` and calls ``AbstractFlashSession.transfer_everything``. This
method creates an ``AsyncFirmwareBlocksIterator`` instance which retrieves
blocks from each ``Firmware``. ``AbstractFlashSession.transfer_everything`` then
calls the sub-class' ``AbstractFlashSession.send_file_block`` implementation to
send the block to the device. When all the blocks have been sent, flashing is
complete.

The provided implementations are:

* ``GdbFlasher`` - use ``gdb-multiarch`` to write blocks directly to memory
  addresses - supports additional files
* ``IrisFlasher`` - use the ``Iris`` library to write blocks directly to
  memory - supports additional files
* ``Mbedflasher`` - use ``mbed_flasher`` to flash to ROM - only supports
  flashing a single firmware
* ``NullFlasher`` (for testing) - does nothing

Device Registry
---------------

A device registry provides the means to discover devices connected to the
system.

Device registries implement ``AbstractDeviceRegistry`` which provides the means
to discover and enumerate devices.

There are two implementations:

* ``FakeDeviceRegistry``, which always "discovers" a fake serial device and a
  fake socket device
* ``RealDeviceRegistry``, which discovers any mbed-compatible boards connected
  to the host machine. FVPs are not discovered - if FVPs are to be used, they
  may be constructed manually using ``DeviceFactory``

Host
----

The ``Host`` class provides higher-level access to devices discovered using the
registry. Using the ``Host`` the user can connect to, flash and communicate with
a cluster of devices.

Test
----

The ``test`` package contains the ``pytest`` integration for ``pyedmgr`` and is
the subject of `part two of the tutorial <./tutorial2.html>`__
