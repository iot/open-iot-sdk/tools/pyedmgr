# Copyright (c) 2022, Arm Limited and Contributors. All rights reserved.
# SPDX-License-Identifier: Apache-2.0
"""
FakeDeviceRegistry.
        """

from .abstract import AbstractDeviceRegistry
from ..device import DeviceFactory


class FakeDeviceRegistry(AbstractDeviceRegistry):
    """
    Device registry that returns fakes.
    """

    # pylint:disable=unused-argument
    def __init__(self, **kwargs):
        device_factory = DeviceFactory()
        self._devices = [device_factory.fake_serial(), device_factory.fake_socket()]

    # pylint:enable=unused-argument

    @property
    def devices(self):
        return list(self._devices)

    async def discover_devices(self):
        return list(self._devices)

    async def enumerate_devices(self):
        return list(self._devices)
