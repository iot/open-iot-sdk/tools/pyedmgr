# Copyright (c) 2022, Arm Limited and Contributors. All rights reserved.
# SPDX-License-Identifier: Apache-2.0
"""
RealDeviceRegistry.
        """
import asyncio
from functools import partial
import logging
from typing import List

import mbed_lstools

from .abstract import AbstractDeviceRegistry
from ..device import DeviceFactory

__all__ = ["RealDeviceRegistry"]

# pylint:disable=C0103
# pylint:disable=W0603
mbed_ls = None


async def list_boards(platforms_supported):
    """
    Retrieve available mbed boards.
    """
    global mbed_ls
    if not mbed_ls:
        mbed_ls = mbed_lstools.create(list_unmounted=True)

    filt = lambda _: True
    if platforms_supported:
        filt = lambda m: m["platform_name"] in platforms_supported

    return await asyncio.get_running_loop().run_in_executor(
        None, partial(mbed_ls.list_mbeds, filter_function=filt)
    )


# pylint:enable=C0103
# pylint:enable=W0603


class RealDeviceRegistry(AbstractDeviceRegistry):
    """
    Device registry that registers physical boards"""

    def __init__(
        self,
        device_factory: DeviceFactory,
        platforms_supported: List[str] = None,
        logger: logging.Logger = None,
    ):
        """
        Initialise registry.

        :param device_factory: Device factory.
        :param platforms_supported: Optional filtering by platform_name/FVP name.
        :param logger: Optional logger.
        """
        self._device_factory = device_factory
        self._platforms_supported = (
            set(platforms_supported) if platforms_supported else set()
        )
        self._logger = logger if logger else logging.getLogger("registry")
        self._boards = {}

    @property
    def devices(self):
        return list(self._boards.values())

    async def discover_devices(self):
        self._logger.debug("Discovering devices")

        for board in await list_boards(self._platforms_supported):
            self._boards[board["target_id"]] = self._device_factory.load_mbed(**board)
        self._logger.debug(f"Discovered {len(self._boards)} boards")

        return self.devices

    async def enumerate_devices(self):
        if not self._boards:
            return await self.discover_devices()
        return self.devices
