# Copyright (c) 2022, Arm Limited and Contributors. All rights reserved.
# SPDX-License-Identifier: Apache-2.0
"""
AbstractDeviceRegistry.
        """
from abc import ABC, abstractmethod
import re
from typing import Iterable, Union

from ..device import Device

NAME_REGEX = re.compile(r"([a-z])([A-Z])")


class AbstractDeviceRegistry(ABC):
    """
    Interface implemented by device registries.
    """

    @property
    @abstractmethod
    def devices(self) -> Union[None, Iterable[Device]]:
        """
        Retrieve previously discovered devices if any.
        """
        raise NotImplementedError()

    @abstractmethod
    async def discover_devices(self) -> Iterable[Device]:
        """
        Perform device discovery, setting the devices property and returning them. Previously discovered devices
        are returned as well.
        """
        raise NotImplementedError()

    @abstractmethod
    async def enumerate_devices(self) -> Iterable[Device]:
        """
        Perform device discovery if it hasn't been performed before, setting the devices property and returning
        them.
        """
        raise NotImplementedError()

    def __str__(self) -> str:
        name = NAME_REGEX.sub("$1 $2", type(self).__name__)
        num = len(self.devices) if self.devices else 0
        return f"{name} [{num} devices]"


__all__ = ["AbstractDeviceRegistry"]
