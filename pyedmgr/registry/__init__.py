# Copyright (c) 2022, Arm Limited and Contributors. All rights reserved.
# SPDX-License-Identifier: Apache-2.0
"""
Device registries.
"""
from .abstract import *
from .fake import *
from .real import *
