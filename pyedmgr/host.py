# Copyright (c) 2022, Arm Limited and Contributors. All rights reserved.
# SPDX-License-Identifier: Apache-2.0

"""
Host class
"""

import asyncio
import atexit
from enum import Enum
import logging
import traceback
from typing import Any, Callable, Coroutine, List, Mapping, Set, Tuple, Union

from .device import Device, DeviceFactory
from .firmware import Firmware
from .registry import AbstractDeviceRegistry, RealDeviceRegistry

__all__ = ["ConnectionFailureBehaviour", "Host"]


class ConnectionFailureBehaviour(Enum):
    """
    Action to take when a device fails to connect.
    """

    WARN = 0
    IGNORE = 1
    FAIL = 2


class Host:
    """
    Host - find, connect, flash and communicate with devices
    """

    def default(**kwargs):
        """
        Construct a Host with RealDeviceRegistry. kwargs accepts all keyword arguments for RealDeviceRegistry and Host.
        """
        allowed_kwargs = {
            "registry",
            "factory",
            "platforms_supported",
            "logger",
            "behaviour",
        }

        for kwarg in kwargs:
            if kwarg not in allowed_kwargs:
                raise ValueError(f"Unsupported keyword argument {kwarg}")

        device_registry = kwargs.get("registry")
        logger = kwargs.get("logger")
        if not device_registry:
            device_factory = kwargs.get("factory")
            if not device_factory:
                device_factory = DeviceFactory()
            platforms_supported = kwargs.get("platforms_supported")
            device_registry = RealDeviceRegistry(
                device_factory, platforms_supported=platforms_supported, logger=logger
            )
        behaviour = kwargs.get("behaviour")
        return Host(device_registry, behaviour=behaviour, logger=logger)

    def __init__(
        self,
        registry: AbstractDeviceRegistry,
        behaviour: ConnectionFailureBehaviour = None,
        logger: logging.Logger = None,
    ):
        """
        Initialise host.

        :param registry: The registry to use for device discovery.
        :param behaviour: The action to take when a device fails to connect. The default is to issue a warning.
        :param logger: An optional logger.

        """
        if not isinstance(registry, AbstractDeviceRegistry):
            raise TypeError("registry")
        self._registry = registry
        self._behaviour = behaviour if behaviour else ConnectionFailureBehaviour.WARN
        self._logger = logger if logger else logging.getLogger("host")
        self._devices: List[Device] = []
        self._flashed: List[Device] = []
        self._logger.debug(f"Using registry {type(self._registry).__name__}")

    @property
    def connected_devices(self) -> List[Device]:
        """
        Gets the currently connected devices.
        """
        return list(self._devices)

    @property
    def registry(self) -> AbstractDeviceRegistry:
        """
        Gets the registry.
        """
        return self._registry

    async def establish_connections(self) -> List[Device]:
        """
        Establishes connections to all devices enumerated by the registry and returns the connected devices.
        """
        devices = await self._registry.enumerate_devices()
        if not devices:
            self._logger.debug("No devices available")
        for device in devices:
            if device in self._devices:
                continue
            self._logger.debug("Attempting to connect to {}".format(device.name))
            try:
                await device.connect()
            # pylint:disable=broad-except
            except Exception as e:
                # pylint:enable=broad-except
                self._handle_conn_fail(device, e)
            else:
                self._devices.append(device)
                self._logger.debug("{} connection established".format(device.name))
        return self._devices

    async def disconnect_all_devices(self):
        devices = list(self._devices)
        self._logger.debug(f"Disconnecting {len(devices)} devices")
        self._flashed.clear()
        self._devices.clear()
        for device in devices:
            await device.disconnect(shutdown=True)

    async def flash_connected_devices(
        self, firmware: List[Firmware]
    ) -> Tuple[List[Device], List[Device]]:
        """
        Allocate and flash firmware to devices returning a tuple of devices that WERE and WERE NOT flashed
        """
        jobs = []

        if len(firmware) > len(self._devices):
            raise RuntimeError("There are more firmwares than devices")

        unflashed_devices = list(self._devices)
        flashed_devices = []

        self._logger.debug(
            "Allocating {} firmwares to {} devices".format(
                len(firmware), len(self._devices)
            )
        )
        self._allocate_matching_devices_firmware(firmware, unflashed_devices, jobs)

        if not jobs:
            self._logger.warning("No devices allocated for flash")
            return [], unflashed_devices

        self._logger.info(
            "Flashing {} firmwares to {} devices".format(len(firmware), len(jobs))
        )
        await self._flash_allocated_devices_firmware(jobs, flashed_devices)

        self._flashed += flashed_devices
        return flashed_devices, unflashed_devices

    async def disconnect_unflashed(self):
        """
        Disconnect devices that were not flashed.
        """
        unflashed = []
        for dev1 in self._devices:
            found = False
            for dev2 in self._flashed:
                if dev1 == dev2:
                    found = True
                    break
            if not found:
                unflashed.append(dev1)

        if not unflashed:
            return

        self._logger.info("Disconnecting {} unused devices".format(len(unflashed)))
        for device in unflashed:
            await device.disconnect(shutdown=True)
            self._devices.remove(device)
            self._logger.debug("{} disconnected".format(device.name))

    async def communicate(
        self,
        read_cb: Callable[[Device, bytes], Coroutine[Any, Any, Any]],
        readuntil: Union[bytes, int] = b"\n",
    ):
        """
        Communicate with devices in a loop.

        :param read_cb: Function to call with bytes received from devices. It can raise StopAsyncIteration to end the
        loop.
        :param readuntil: Either \n to read lines or a number of bytes to read.
        """
        comms: Mapping[Device, asyncio.Task] = {}
        tasks: Set[asyncio.Task] = set()
        read_fn: Callable[[Device], Coroutine[Any, Any, bytes]] = None

        if readuntil in ("\n", b"\n"):
            read_fn = lambda d: d.channel.readline_async()
        elif readuntil > 0:
            read_fn = lambda d: d.channel.read_async(readuntil)
        else:
            raise ValueError(f"Invalid value for readuntil: {readuntil}")

        self._logger.info(
            "Entering loop for {} connected devices".format(len(self._flashed))
        )

        def cancel_tasks():
            loop = None
            try:
                loop = asyncio.get_running_loop()
            except RuntimeError:
                pass
            else:
                while tasks:
                    for task in tasks:
                        if task.done() or task.cancelled():
                            tasks.remove(task)
                            break
                        task.cancel()
                        try:
                            loop.run_until_complete(task)
                        except asyncio.exceptions.CancelledError:
                            pass

        atexit.register(cancel_tasks)
        running = True
        while running:
            for device in self._flashed:
                task = comms.get(device)
                if not task:
                    task = asyncio.create_task(read_fn(device))
                    comms[device] = task
                    tasks.add(task)
                elif task.done():
                    line = await task
                    del comms[device]
                    tasks.remove(task)
                    try:
                        await read_cb(device, line)
                    except StopIteration:
                        running = False
                        break
                    except StopAsyncIteration:
                        running = False
                        break
                else:
                    await asyncio.sleep(0)

    def __str__(self):
        return f"{type(self).__name__} <registry={self._registry},connected={len(self._devices)}>"

    def _handle_conn_fail(self, device: Device, e: Exception):
        e_args = " ".join(map(str, e.args))
        if self._behaviour == ConnectionFailureBehaviour.WARN:
            if "No config" not in e_args:
                self._logger.debug(traceback.format_exc())
            self._logger.warning(
                "Unable to connect to {} because {}".format(device.name, e_args)
            )
        elif self._behaviour == ConnectionFailureBehaviour.IGNORE:
            if "No config" not in e_args:
                self._logger.debug(traceback.format_exc())
                self._logger.warning(
                    "Unable to connect to {} because {}".format(device.name, e_args)
                )
        elif self._behaviour == ConnectionFailureBehaviour.FAIL:
            raise e
        else:
            raise ValueError(
                f"Bug: no case for ConnectionFailureBehaviour={self._behaviour}"
            ) from e

    def _allocate_matching_devices_firmware(
        self,
        firmware: List[Firmware],
        unflashed_devices: List[Device],
        jobs: List[Tuple[Firmware, Device, asyncio.Task]],
    ) -> List[Firmware]:
        """
        Allocate Firmware to Devices with matching firmware types.
        """
        remaining_firmware = []
        for fw in firmware:
            allocd = False
            for dev in unflashed_devices:
                if fw.type in dev.allowed_firmware_types:
                    task = asyncio.create_task(dev.flash(fw))
                    types = "|".join(map(lambda t: t.name, dev.allowed_firmware_types))
                    self._logger.debug(
                        f"{fw.path} ({fw.type.name}) allocated to {dev.name} ({types})"
                    )
                    jobs.append((fw, dev, task))
                    unflashed_devices.remove(dev)
                    allocd = True
                    break
            if not allocd:
                remaining_firmware.append(fw)
        if remaining_firmware:
            remaining_firmware = ", ".join(map(lambda f: f.path, remaining_firmware))
            raise ValueError(
                f"The following firmware could not be flashed: {remaining_firmware}"
            )

    # pylint:disable=no-self-use

    async def _flash_allocated_devices_firmware(
        self, jobs: List[Tuple[Firmware, Device, asyncio.Task]], flashed: List[Device]
    ) -> None:
        await asyncio.sleep(0)  # Flashing starts now
        while jobs:
            fw, dev, task = jobs.pop(0)
            if task.done():
                await task
                flashed.append(dev)
            else:
                jobs.append((fw, dev, task))
            await asyncio.sleep(0)
