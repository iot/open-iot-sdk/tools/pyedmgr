# Copyright (c) 2022, Arm Limited and Contributors. All rights reserved.
# SPDX-License-Identifier: Apache-2.0
"""
Initialise pyedmgr.
        """
from datetime import datetime, timedelta
import logging
import os
import sys
from typing import List, Optional, Tuple

from . import const

__all__ = ["initialise"]


class _Logfilter(logging.Filter):
    _FILTER: List[Tuple[str, Optional[timedelta]]] = [
        ("Using selector: EpollSelector", None),
        (
            "Could not get serial devices by id. This could be because your Linux distribution does not use udev, or does not create /dev/serial/by-id symlinks.",
            None,
        ),
        ("Use 'mbedfm' to list all the available Fast Models", None),
    ]

    def __init__(self):
        self.history = {}

    def filter(self, record):
        message = record.getMessage()

        # Test whether the message should be filtered.
        found = None
        for phrase, period in self._FILTER:
            if phrase in message:
                found = (phrase, period)
                break

        # If the record matched no filters, log it.
        if not found:
            return True

        # If the record matched and the interval is None/0, always filter the record.
        _, interval = found
        if not interval:
            return False

        # If the record matched and has a non-zero interval, log only if the interval has elapsed.
        now = datetime.utcnow()
        if message in self.history:
            age = now - self.history[message]
            if age < interval:
                return False

        # Update the history.
        self.history[message] = now
        return True


def initialise():
    # By adding the filter to the root logger, it will be copied into all loggers created after this module is
    # imported.
    for handler in logging.getLogger().handlers:
        handler.addFilter(_Logfilter())

    dir_list = {}

    def _cached_listdir(path):
        nonlocal dir_list
        if path not in dir_list:
            if os.path.isdir(path):
                dir_list[path] = set(os.listdir(path))
            else:
                dir_list[path] = set()
        return dir_list[path]

    def _shell_find(binary):
        """
        Get directory within PATH which contains binary, or None.
        """
        for path in os.environ["PATH"].split(os.pathsep):
            if binary in _cached_listdir(path):
                return path
        return None

    def _prepend_paths(var: str, new_paths: List[str]) -> List[str]:
        var_paths = os.environ.get(var, "").split(os.pathsep)
        for path in new_paths:
            if os.path.isdir(path):
                var_paths.insert(0, path)
        os.environ[var] = os.pathsep.join(var_paths)

    # Retrieve installation paths from environment variables or by searching PATH.
    const.FVP_INSTALL_PATHS = list(
        filter(
            lambda p: p is not None and os.path.exists(p),
            map(
                lambda pair: os.environ.get(
                    pair[0] + "_INSTALL_PATH", _shell_find(pair[1])
                ),
                const.FVP_ALIAS.items(),
            ),
        )
    )

    const.IRIS_INSTALL_DIR = os.environ.get("IRIS_INSTALL_DIR", None)

    if const.FVP_INSTALL_PATHS:
        if not const.IRIS_INSTALL_DIR:
            # Try to deduce Iris path it from FVP_INSTALL_PATHS.
            _existent_paths = list(filter(os.path.isdir, const.FVP_INSTALL_PATHS))
            if _existent_paths:
                const.IRIS_INSTALL_DIR = os.path.join(
                    _existent_paths[0], "..", "..", "Iris", "Python"
                )

        # Ensure FVP binaries are in PATH and LD_LIBRARY_PATH
        _prepend_paths("PATH", const.FVP_INSTALL_PATHS)
        _prepend_paths("LD_LIBRARY_PATH", const.FVP_INSTALL_PATHS)

    # Add Iris to modules path if it exists.
    if os.path.isdir(const.IRIS_INSTALL_DIR):
        sys.path.append(const.IRIS_INSTALL_DIR)
