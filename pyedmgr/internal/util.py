# Copyright (c) 2022, Arm Limited and Contributors. All rights reserved.
# SPDX-License-Identifier: Apache-2.0
"""
General utilities.
        """

import asyncio
from collections import defaultdict
import logging
import os
import re
from typing import Any, Callable, Coroutine, List, Mapping, Tuple, Union

import nest_asyncio

__all__ = [
    "Expect",
    "LogStream",
    "dict_rename",
    "dict_remove",
    "asyncify",
    "syncify",
    "ensure_done",
    "envify",
    "get_null_logger",
]


class Expect:
    """
    Intentful runtime preconditions.
    """

    @classmethod
    def has_type(
        cls, obj: object, type_: Union[type, tuple], err: Union[str, Exception]
    ):
        """
        Assert that the object has an expected type.
        """
        if not isinstance(obj, type_):
            cls._raise(err, TypeError)

    @classmethod
    def has_key(cls, d: dict, k: str, err: Union[str, Exception]):
        """
        Assert that the dictionary has an expected key.
        """
        if k not in d:
            cls._raise(err, KeyError)

    @classmethod
    def has_key_of_type(
        cls, d: dict, k: str, type_: Union[type, tuple], err: Union[str, Exception]
    ):
        """
        Assert that the dictionary has an expected key and that the value has an expected type.
        """
        cls.has_key(d, k, err)
        val = d[k]
        cls.has_type(val, type_, err)

    @classmethod
    def is_tuple_of(cls, tup: tuple, types: tuple, err: Union[str, Exception]):
        """
        Assert that the values of the tuple 'tup' match the types in the tuple 'types' in type, number and order.
        """
        if len(tup) != len(types):
            cls._raise(err, ValueError)
        for i, val in enumerate(tup):
            Expect.has_type(val, types[i], err)

    @classmethod
    def _raise(cls, err: Union[str, Exception], fallback: Exception):
        if isinstance(err, Exception):
            raise err
        raise fallback(err)


class LogStream:
    """
    Fake file-like object that logs to a logger. The stream is buffered and the buffer is flushed when a newline is
    encountered and when the object is destroyed. Every line written to the buffer is emitted as a separate log event so
    that lines are all indented to the same position, e.g. write('hello\nworld') results in a log like:
    INFO hello
    INFO world
    With 'world' being printed when the object is destroyed as there is no trailing newline.
    """

    def __init__(self, level: Union[int, str], logger: logging.Logger = None):
        self.level = level
        self.logger = logger if logger else logging.getLogger("fake")
        self._buf = ""
        self._match: Mapping[str, List[Callable]] = defaultdict(list)

    def __del__(self):
        # We don't know if the callbacks still exist during object destruction.
        self._match.clear()

        self._flush(force=True)

    def match(self, pattern: str, callback: Callable):
        """
        Add a callback to be called when a log message is flushed that contains the pattern. The callbacks will *not*
        be called when the last part of the stream is flushed in the destructor.
        """
        self._match[pattern].append(callback)

    def write(self, msg: str):
        """
        Write to the log.
        """
        self._buf += msg
        self._flush()

    def _flush(self, force=False):
        # Do nothing if buffer is empty. Do nothing if buffer contains an incomplete line, unless force is enabled.
        lines = self._buf.splitlines(keepends=True)
        if not lines or (not force and len(lines) == 1 and not lines[0].endswith("\n")):
            return

        # Log the lines. Skip blanks.
        for line in lines[:-1]:
            self._log(line)

        # In force mode, log the incomplete line and clear the buffer, otherwise put the incomplete line back in the
        # buffer.
        if lines[-1].endswith("\n") or force:
            self._log(lines[-1])
            self._buf = ""
        else:
            self._buf = lines[-1]

    def _log(self, line):
        line = line.strip()
        if not line:
            return

        for pattern, callbacks in self._match.items():
            if re.match(pattern, line):
                for callback in callbacks:
                    callback(line)

        level = self.level
        if "error" in line or "ERROR" in line:
            level = logging.ERROR
        elif "warn" in line or "WARN" in line:
            level = logging.WARNING
        self.logger.log(level, line)


def dict_rename(d: Mapping[Any, Any], key_from: Any, key_to: Any) -> bool:
    """
    If there is a value for 'key_from' in 'd', remove it and reinsert it under 'key_to'. Otherwise do nothing.
    The dictionary is changed in-place.
    :param d: The dictionary.
    :param key_from: The key to rename.
    :param key_to: The new name for the key.
    :return: True if a key was renamed, otherwise False.
    """
    value = d.get(key_from)
    if value is None:
        return False
    d[key_to] = value
    del d[key_from]
    return True


def dict_remove(d: Mapping[Any, Any], key: Any) -> bool:
    """
    Remove 'key' from 'd' and return True if present, otherwise return False."""
    if key in d:
        del d[key]
        return True
    return False


async def asyncify(
    fn: Callable, *args: Tuple[Any], **kwargs: Mapping[str, Any]
) -> Coroutine[Any, Any, Any]:
    """
    Run a synchronous function asynchronously.
    """

    # Wrap fn because run_in_executor doesn't take keyword arguments.
    def wrapper():
        return fn(*args, **kwargs)

    return await asyncio.get_running_loop().run_in_executor(None, wrapper)


def syncify(
    fn: Callable[..., Coroutine[Any, Any, Any]],
    *args: Tuple[Any],
    **kwargs: Mapping[str, Any],
) -> Any:
    """
    Start a coroutine and synchronously wait for it to finish.
    """
    loop = None
    try:
        loop = asyncio.get_running_loop()
    except RuntimeError:
        loop = asyncio.new_event_loop()
    nest_asyncio.apply(loop)
    return loop.run_until_complete(fn(*args, **kwargs))


async def ensure_done(task: asyncio.Task):
    """
    Ensure task is done and cancel it if not.
    """
    if not task.done():
        task.cancel()
    try:
        return await task
    except asyncio.exceptions.CancelledError:
        return None


def envify(s):
    """
    Replace substrings enclosed by {{ and }} with values from the environment variable named by the substring,
    e.g. {{PWD}} is replaced with the value of PWD.
    """
    result = ""
    for elem in s.split("}}"):
        if elem.startswith("{{"):
            val = os.getenv(elem[2:])
            if val:
                result += val
            else:
                raise RuntimeError(f"Environment variable {elem[2:]} unset or empty")
        else:
            result += elem
    return result


class _NullLogger(logging.Logger):
    def __init__(self):
        super().__init__("null")

    def log(self, *args, **kwargs):
        pass

    def debug(self, *args, **kwargs):
        pass

    def info(self, *args, **kwargs):
        pass

    def warn(self, *args, **kwargs):
        pass

    def warning(self, *args, **kwargs):
        pass

    def error(self, *args, **kwargs):
        pass

    def critical(self, *args, **kwargs):
        pass


def get_null_logger():
    return _NullLogger()
