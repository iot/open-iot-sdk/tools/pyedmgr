# Copyright (c) 2022-23, Arm Limited and Contributors. All rights reserved.
# SPDX-License-Identifier: Apache-2.0

"""
Constant values.
        """

from typing import List, Optional

__all__ = [
    "DEFAULT_HOST",
    "DEFAULT_PORT",
    "DEFAULT_BAUD",
    "DEFAULT_TIMEOUT",
    "FVP_ALIAS",
    "FVP_INSTALL_PATHS",
    "IRIS_INSTALL_DIR",
]

DEFAULT_HOST = "localhost"
DEFAULT_PORT = 54321
DEFAULT_BAUD = 115200
DEFAULT_TIMEOUT = None

# Aliases for FVP binaries.
FVP_ALIAS = {
    "FVP_CS300": "FVP_Corstone_SSE-300_Ethos-U55",
    "FVP_CS300_U55": "FVP_Corstone_SSE-300_Ethos-U55",
    "FVP_CS300_U65": "FVP_Corstone_SSE-300_Ethos-U65",
    "FVP_CS310": "FVP_Corstone_SSE-310",
    "FVP_CS310_U55": "FVP_Corstone_SSE-310",
    "FVP_CS310_U65": "FVP_Corstone_SSE-310_Ethos-U65",
    "VHT_CS300": "VHT_Corstone_SSE-300_Ethos-U55",
    "VHT_CS300_U55": "VHT_Corstone_SSE-300_Ethos-U55",
    "VHT_CS300_U65": "VHT_Corstone_SSE-300_Ethos-U65",
    "VHT_CS310": "VHT_Corstone_SSE-310",
    "VHT_CS310_U55": "VHT_Corstone_SSE-310",
    "VHT_CS310_U65": "VHT_Corstone_SSE-310_Ethos-U65",
}

FVP_INSTALL_PATHS: List[str] = []

IRIS_INSTALL_DIR: Optional[str] = None
