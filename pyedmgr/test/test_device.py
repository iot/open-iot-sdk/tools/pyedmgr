# Copyright (c) 2022, Arm Limited and Contributors. All rights reserved.
# SPDX-License-Identifier: Apache-2.0

"""
Test device.
"""

import logging
from typing import Any, Coroutine, Iterable, List, Optional, Tuple, Union

from .primitive import FirmwareOrFileAddressList
from ..channel import AbstractChannel
from ..controller import AbstractController
from ..device import AbstractDevice, Device
from ..firmware import Firmware, FirmwareType
from ..internal import envify, Expect


class TestDevice(AbstractDevice):
    """
    Test device.
    """

    __test__ = False

    def __init__(
        self,
        device: Device,
        *firmware_list: FirmwareOrFileAddressList,
        exact_firmware=None,
        exact_additional_files=None,
    ):
        self._device = device
        if exact_firmware is None:
            self._firmware = [
                (envify(f) if isinstance(f, str) else f)
                for f in firmware_list
                if isinstance(f, (str, Firmware))
            ][0]
            self._additional_files = [
                (envify(f[0]), f[1]) for f in firmware_list if isinstance(f, tuple)
            ]
            if isinstance(self._firmware, str):
                self._firmware = Firmware(self._firmware)
            else:
                Expect.is_tuple_of(
                    self._firmware,
                    (str, int),
                    "Items in firmware list must be Firmware, str or (str,int)",
                )
        else:
            self._firmware = exact_firmware
            self._additional_files = exact_additional_files

    @property
    def underlying_device(self) -> Device:
        """
        Gets the underlying instance of class Device.
        """
        return self._device

    @property
    def firmware(self) -> Firmware:
        """
        Gets the firmware that will be/has been flashed.
        """
        return self._firmware

    @property
    def additional_files(self) -> List[Tuple[str, int]]:
        """
        Gets the additional files that will be/have been flashed.
        """
        return self._additional_files

    @property
    def name(self) -> str:
        """
        Gets a friendly name for the device.
        """
        return self._device.name

    @property
    def allowed_firmware_types(self) -> FirmwareType:
        """
        Indicates whether the device requires firmware to have a symbol table or be a flat ELF file.
        """
        return self._device.allowed_firmware_types

    @property
    def connected(self) -> bool:
        """
        Indicates whether the device is connected.
        """
        return self._device.connected

    @property
    def channel(self) -> AbstractChannel:
        """
        Gets the communication channel.
        """
        return self._device.channel

    @property
    def controller(self) -> AbstractController:
        """
        Gets the communication channel.
        """
        return self._device.controller

    @property
    def clonable(self) -> bool:
        return self._device.clonable

    async def communicate(
        self,
        expectations:Iterable[Tuple[Optional[str], Optional[str]]],
        encoding:Union[None,str,Tuple[str,str]]=None,
        logger:Optional[logging.Logger]=None
    ):
        """
        Communicate with the device returning the first incorrect expectation.

        :param expectations: An iterable of pairs, where the left item is None or a string/bytes to send to the device,
        and the right item is None or a string/bytes that should be received. Inbound expectations are assumed to be
        whole lines so shouldn't end with '\n'.

        :param encoding: Encoding to use when expectations are strings. If not given, 'utf-8' is used by default.
        May be a string naming the codec or a tuple of (codec, errors) where errors is the desired error handling mode
        (see bytes.decode/str.encode; "strict" is used by default).

        :return: The index of the first expectation that was not met, or len(expectations) if there were no incorrect
        expectations.

        :example:
            # In this example the device is expected to first send 'Ready' and then respond 'bar' when 'foo' is sent.
            # Bytes and strings can be intermixed in the expectations array as show below.
            i = await device.communicate([(None,'Ready'), ('foo',b'bar')])
        """
        if not encoding:
            encoding = "utf-8"
        if isinstance(encoding, str):
            encoding = (encoding, "strict")

        index = 0
        for send, expect in expectations:
            if send is not None:
                if logger:
                    logger.debug(f"> {send}")
                if isinstance(send, str):
                    send = send.encode(*encoding)
                self.channel.write_async(send)

            if expect is not None:
                line = await self.channel.readline_async()
                if isinstance(expect, str):
                    line = line.decode(*encoding).strip()
                if logger:
                    logger.debug(f"{self.channel} < {line}")
                if expect in line:
                    index += 1

        return index

    async def connect(self) -> Coroutine[Any, Any, None]:
        return await self._device.connect()

    async def disconnect(self, shutdown=False) -> Coroutine[Any, Any, None]:
        return await self._device.disconnect(shutdown)

    async def flash(
        self, firmware: Firmware, additional_files: List[Tuple[str, int]] = None
    ) -> Coroutine[Any, Any, None]:
        return await self._device.flash(firmware, additional_files)

    async def reset(self) -> Coroutine[Any, Any, None]:
        return await self._device.reset()

    async def clone(self):
        device = await self._device.clone()
        return TestDevice(
            device,
            exact_firmware=self._firmware,
            exact_additional_files=self._additional_files,
        )

    async def _self_flash(self):
        """
        Flash the device with the preset firmware.
        """
        if not self._device.can_flash_with(self.firmware):
            raise ValueError(
                f"Invalid firmware '{self.firmware.path}' of type '{self.firmware.type.name}' "
                + f"for device '{self.name}'"
            )
        await self.flash(self.firmware, self.additional_files)
