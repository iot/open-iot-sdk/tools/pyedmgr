# Copyright (c) 2022, Arm Limited and Contributors. All rights reserved.
# SPDX-License-Identifier: Apache-2.0

"""
CI test system flags.
        """

SETUP_LOGGING = True

LOG_FORMAT = "%(levelname)-8s %(filename)s:%(lineno)d %(funcName)s %(name)s %(message)s"
