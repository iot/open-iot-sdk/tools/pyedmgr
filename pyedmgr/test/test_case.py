# Copyright (c) 2022, Arm Limited and Contributors. All rights reserved.
# SPDX-License-Identifier: Apache-2.0

"""
Test case and test case context.
        """

from typing import Mapping, Union

from .primitive import FirmwareOrFileAddressList
from .test_case_context import TestCaseContext
from ..host import Host, ConnectionFailureBehaviour


class TestCase:
    """
    Test case context manager. Enter a context using `async with` and receive a list of flashed TestDevices.

    :param device_firmware_mapping: A mapping (such as dict or list of tuples) of strings (device IDs such as
    FVP_Corstone_SSE-300_Ethos-U55) to dictionaries mapping parameters ('firmware' or 'config') to their values.
    """

    __test__ = False

    @classmethod
    def parse(cls, test_case_descriptor):
        from .test_case_parser import TestCaseParser

        return TestCaseParser(test_case_descriptor).parse()

    def __init__(
        self,
        device_firmware_mapping: Mapping[
            str, Union[FirmwareOrFileAddressList, str]
        ] = None,
    ):
        self._device_firmware_mapping = device_firmware_mapping
        self._ctx: TestCaseContext = None

    async def __aenter__(self):
        if self._ctx:
            raise RuntimeError("Context already activate")

        host = Host.default(behaviour=ConnectionFailureBehaviour.IGNORE)

        self._ctx = TestCaseContext(host, self._device_firmware_mapping)

        await self._ctx._setup()

        return self._ctx

    async def __aexit__(self, ex_type, ex_val, ex_tb):
        await self._ctx._cleanup()

        del self._ctx
        self._ctx = None
