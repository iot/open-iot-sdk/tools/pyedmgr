# Copyright (c) 2022, Arm Limited and Contributors. All rights reserved.
# SPDX-License-Identifier: Apache-2.0

"""
Primitive types.
        """

from typing import List, Tuple, Union

FirmwareOrFileAddress = Union[str, Tuple[str, int]]
FirmwareOrFileAddressList = List[FirmwareOrFileAddress]
