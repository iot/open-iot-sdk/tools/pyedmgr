# Copyright (c) 2022, Arm Limited and Contributors. All rights reserved.
# SPDX-License-Identifier: Apache-2.0

"""
CI test.
"""

import coloredlogs
import logging
import os
import sys

import pytest

import pytest_asyncio

from .primitive import *
from .test_case import *
from .test_device import *
from .test_case_parser import *
from . import flags


if flags.SETUP_LOGGING:
    formatter = None
    if os.isatty(sys.stdout.fileno()):
        formatter = coloredlogs.ColoredFormatter(flags.LOG_FORMAT)
    else:
        formatter = logging.Formatter(flags.LOG_FORMAT)

    loggers = [logging.getLogger()]
    loggers = loggers + [
        logging.getLogger(name) for name in logging.root.manager.loggerDict
    ]
    for logger in loggers:
        for handler in logger.handlers:
            handler.setFormatter(formatter)


@pytest.fixture
def fixture_test_case(request):
    """
    Fixture for creating test cases.

    :return:
    1. A single TestCase when `request.param` is a list, tuple or dict which maps from str to FirmwareOrAddressList, or
       a string naming a file which contains a JSON dictionary describing such a mapping.
    2. An iterable of TestCases when `request.param` names a file containing a JSON array of mappings.

    :example:
        # In this example there will be one test using two FVPs. Upon entering the context both FVPs are flashed with
        # the firmware.elf binary. The returned context provides access to the devices.
        @pytest.mark.asyncio
        @pytest.mark.parametrize(
            'fixture_test_case',
            [
                {
                    'FVP_Corstone_SSE-300_Ethos-U55': {
                        1: {'firmware': ['firmware.elf'], 'config': 'MPS3.conf'},
                        2: ['firmware.elf'],
                    }
                }
            ],
            indirect=True
        )
        async def test_example_test(fixture_test_case:TestCase):
            async with fixture_test_case as context:
                print(len(context.allocated_devices), 'devices allocated')
                for device in context.allocated_devices:
                    print(device.name, ':', device.firmware.path)
    """
    cases = TestCase.parse(request.param)
    if len(cases) == 1:
        return cases[0]
    return cases

@pytest_asyncio.fixture
async def test_case_context(request):
    """
    Like fixture_test_case, but yields a TestCaseContext that has already been entered, removing the need for the
    "async with" block in a test.

    :yields: A TestCaseContext for each test case parsed from the request.

    :example:
        # In this example there will be one test using two FVPs. Upon entering the context both FVPs are flashed with
        # the firmware.elf binary. The returned context provides access to the devices.
        @pytest.mark.asyncio
        @pytest.mark.parametrize(
            'test_case_context',
            [
                {
                    'FVP_Corstone_SSE-300_Ethos-U55': {
                        1: {'firmware': ['firmware.elf'], 'config': 'MPS3.conf'},
                        2: ['firmware.elf'],
                    }
                }
            ],
            indirect=True
        )
        async def test_example_test(test_case_context:TestCaseContext):
            print(len(context.allocated_devices), 'devices allocated')
            for device in context.allocated_devices:
                print(device.name, ':', device.firmware.path)
    """
    cases = TestCase.parse(request.param)
    for case in cases:
        async with case as context:
            yield context

@pytest.fixture
def logger():
    logger = logging.getLogger("test")
    return logger
