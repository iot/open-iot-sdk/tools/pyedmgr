# Copyright (c) 2022, Arm Limited and Contributors. All rights reserved.
# SPDX-License-Identifier: Apache-2.0

"""
Test case and test case context.
        """

import asyncio
import os
from typing import List, Mapping, Union

from pyedmgr.device.factory import DeviceFactory

from .primitive import FirmwareOrFileAddressList
from .test_device import TestDevice
from ..host import Host

__all__ = ["TestCaseContext"]


def _is_exe(name):
    for path in os.environ.get("PATH", "").split(os.path.pathsep):
        for ext in ("", ".exe"):
            exe = os.path.join(path, f"{name}{ext}")
            if os.path.exists(exe):
                return exe


class TestCaseContext:
    """
    Test case context.
    """

    __test__ = False

    def __init__(
        self,
        host: Host,
        device_firmware_mapping: Mapping[str, Union[FirmwareOrFileAddressList, str]],
    ):
        self.device_firmware_mapping = device_firmware_mapping
        self._host = host
        self._allocated_devices: List[TestDevice] = []

    def __del__(self):
        for device in self._allocated_devices:
            del device
        del self._allocated_devices
        del self._host

    @property
    def allocated_devices(self) -> List[TestDevice]:
        """
        Gets the devices that were allocated in the test.
        """
        return list(self._allocated_devices)

    @property
    def host(self) -> Host:
        """
        Gets the host.
        """
        return self._host

    # pylint:disable=protected-access
    async def _setup(self):
        """
        Allocate and flash devices.
        """
        await self.host.establish_connections()

        # Allocate devices
        if bool(self.device_firmware_mapping):
            if isinstance(self.device_firmware_mapping, dict):
                self.device_firmware_mapping = self.device_firmware_mapping.items()

            for device_name, device_args in self.device_firmware_mapping:
                config = device_args.get("config")
                firmware = device_args.get("firmware", [])
                args = device_args.get("args", None)
                await self._allocate(device_name, *firmware, config=config, args=args)

        # Flash allocated devices
        tasks: List[asyncio.Task] = []
        for device in self._allocated_devices:
            task = asyncio.create_task(device._self_flash())
            tasks.append(task)

        await asyncio.gather(*tasks)

    # pylint:enable=protected-access

    async def _cleanup(self):
        await self.host.disconnect_all_devices()
        for device in self._allocated_devices:
            await device.disconnect(shutdown=True)
            del device

    async def _allocate(
        self, device_name: str, *firmware, config: str = None, args: List[str] = None
    ):
        """
        Allocate a device by name.
        """
        wildcard = bool(device_name) and all(c == "*" for c in device_name)

        for device in self._host.connected_devices:
            if not wildcard and device_name not in device.name:
                continue

            test_device = TestDevice(device, *firmware)
            self._allocated_devices.append(test_device)
            return

        # Interpret device name as an FVP binary if there is a like-named binary in PATH.
        if _is_exe(device_name):
            device = DeviceFactory().load_fvp(device_name, MPS3=config, args=args)
            await device.connect()
            test_device = TestDevice(device, *firmware)
            self._allocated_devices.append(test_device)
            return

        raise KeyError(f"Could not find a free '{device_name}'")
