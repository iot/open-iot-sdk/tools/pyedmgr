# Copyright (c) 2022, Arm Limited and Contributors. All rights reserved.
# SPDX-License-Identifier: Apache-2.0

from abc import ABC, abstractmethod
import logging
from typing import List, Union

try:
    import ujson as json
    from ujson import JSONDecodeError
except ImportError:
    import json
    from json import JSONDecodeError


from .test_case import TestCase
from ..internal import envify, FVP_ALIAS


class VisitorFactory:
    ...


class AbstractVisitor(ABC):
    def visit(self, x: Union[str, list, dict], path=""):
        result = None

        if isinstance(x, str):
            result = self.visit_string(x, path)
        elif isinstance(x, list):
            result = self.visit_list(x, path)
        elif isinstance(x, dict):
            result = self.visit_dict(x, path)
        else:
            type_x = type(x).__name__
            raise TypeError(f"{path}: Invalid type: {type_x}")

        return result

    @abstractmethod
    def visit_string(self, s: str, path=""):
        raise NotImplementedError()

    @abstractmethod
    def visit_list(self, l: list, path=""):
        raise NotImplementedError()

    @abstractmethod
    def visit_dict(self, d: dict, path=""):
        raise NotImplementedError()


def load_and_parse_json(filename_or_json, path=""):
    if filename_or_json.startswith("@"):
        with open(envify(filename_or_json[1:])) as fp:
            return json.load(fp)
    return json.loads(filename_or_json)


class RootVisitor(AbstractVisitor):
    def __init__(self, factory: VisitorFactory):
        self.factory = factory

    def visit_string(self, s: str, path=""):
        if bool(path):
            raise TypeError(f"{path}: string is not valid here")
        try:
            s = load_and_parse_json(s, path)
        except JSONDecodeError as e:
            raise TypeError(f"{path}: JSON decode error: " + "  ".join(e.args))
        else:
            return self.visit(s, path)

    def visit_list(self, l: list, path=""):
        visitor = self.factory.create_test_case_list_visitor()
        return visitor.visit(l, path)

    def visit_dict(self, d: dict, path=""):
        visitor = self.factory.create_test_case_visitor()
        return visitor.visit(d, path)


class TestCaseListVisitor(AbstractVisitor):
    def __init__(self, factory: VisitorFactory):
        self.factory = factory

    def visit_string(self, s: str, path=""):
        try:
            return load_and_parse_json(s, path)
        except JSONDecodeError as e:
            raise TypeError(f"{path}: JSON decode error: " + "  ".join(e.args))

    def visit_list(self, l: list, path=""):
        if bool(path):
            raise TypeError(f"{path}: list is not valid here")

        res = [None] * len(l)
        for i, thing in enumerate(l):
            res[i] = self.visit(thing, f"{path}[{i}]")
        return res

    def visit_dict(self, d: dict, path=""):
        visitor = self.factory.create_test_case_visitor()
        return visitor.visit(d, path)


class TestCaseVisitor(AbstractVisitor):
    def __init__(self, factory: VisitorFactory):
        self.factory = factory

    def visit_string(self, s: str, path=""):
        try:
            return self.visit(load_and_parse_json(s, path), path)
        except JSONDecodeError:
            raise TypeError(f"{path}: non-JSON, non-@filename string is not valid here")

    def visit_list(self, l: list, path=""):
        raise ValueError(f"{path}: list is not valid here")

    def visit_dict(self, d: dict, path=""):
        # The dict at this point is in one of the following forms:
        #   A: {'FVP_Corstone_SSE-300_Ethos-U55': args_for_fvp}
        #   B: {'FVP_Corstone_SSE-300_Ethos-U55': {1: args_for_fvp1, 2: args_for_fvp_2}}
        #
        # In case A the result is a list containing a single tuple ('FVP_Corstone_SSE-300_Ethos-U55', args_for_fvp). In case B the result
        # is a list containing two tuples: ('FVP_Corstone_SSE-300_Ethos-U55', args_for_fvp_1) and ('FVP_Corstone_SSE-300_Ethos-U55', args_for_fvp_2).
        #
        # Then the args_for_fvp (handled by DeviceSpecVisitor) is in one of the following forms:
        #   A: {'firmware': firmware_list, 'args': arg_list}
        #   B: firwmare_list

        visitor = self.factory.create_device_spec_visitor()
        results = [None] * len(d)
        count = 0

        def add_result(device_name, device_spec):
            nonlocal count
            nonlocal results

            results[count] = (device_name, device_spec)
            count += 1

            # Double the capacity of results when we exceed its current capacity
            if count >= len(results):
                next_results = [None] * (2 * len(results))
                for i, res in enumerate(results):
                    next_results[i] = res
                results.clear()
                results = next_results

        for device_name, device_spec in d.items():
            for alias, real_name in FVP_ALIAS.items():
                if device_name == alias:
                    device_name = real_name
                    break
            device_spec = visitor.visit(device_spec, f"{path}.{device_name}")
            if isinstance(device_spec, str):
                add_result(device_name, {"firmware": [device_spec]})
            elif isinstance(device_spec, list):
                for spec in device_spec:
                    if isinstance(spec, str):
                        spec = {"firmware": [spec]}
                    add_result(device_name, spec)
            else:
                add_result(device_name, device_spec)

        return results[:count]


class DeviceSpecVisitor(AbstractVisitor):
    def __init__(self, factory: VisitorFactory):
        self.factory = factory

    def visit_string(self, s: str, path="") -> Union[str, dict, list]:
        try:
            return self.visit(load_and_parse_json(s, path), path)
        except JSONDecodeError:
            # String literal IS valid here - it names a firmware binary.
            return s

    def visit_list(self, l: list, path=""):
        # If the test spec is just a list, it's a firmware list.
        return {"firmware": list(map(self.envify_strings, l))}

    def visit_dict(self, d: dict, path=""):
        is_number = lambda k: isinstance(k, int) or (
            isinstance(k, str) and k.isnumeric()
        )
        if all(map(is_number, d.keys())):
            return list(map(self.visit, d.values()))

        return self.envify_strings(d)

    def envify_strings(self, x):
        if isinstance(x, str):
            return envify(x)

        if isinstance(x, dict):
            keys = map(self.envify_strings, x.keys())
            values = map(self.envify_strings, x.values())
            return dict(zip(keys, values))

        if hasattr(x, "__iter__"):
            y = map(self.envify_strings, x)
            if isinstance(x, tuple):
                return tuple(y)
            return list(y)

        return x


class VisitorFactory:
    def create_root_visitor(self):
        return RootVisitor(self)

    def create_test_case_list_visitor(self):
        return TestCaseListVisitor(self)

    def create_test_case_visitor(self):
        return TestCaseVisitor(self)

    def create_device_spec_visitor(self):
        return DeviceSpecVisitor(self)


class TestCaseParser:
    __test__ = False

    def __init__(self, source):
        self.source = source

    def parse(self) -> List[TestCase]:
        factory = VisitorFactory()
        visitor = factory.create_root_visitor()

        logging.debug(self.source)
        mappings = visitor.visit(self.source)

        if not mappings:
            return []

        if isinstance(mappings, tuple):
            mappings = [mappings]

        if isinstance(mappings, list) and isinstance(mappings[0], tuple):
            mappings = [mappings]

        results = [None] * len(mappings)
        for i, map_ in enumerate(mappings):
            results[i] = TestCase(map_)

        return results
