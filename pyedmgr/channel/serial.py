# Copyright (c) 2022, Arm Limited and Contributors. All rights reserved.
# SPDX-License-Identifier: Apache-2.0

"""
SerialChannel
"""

import logging
from typing import Any, Coroutine

from aioserial import AioSerial

from .abstract import AbstractChannel


class SerialChannel(AbstractChannel):
    """
    Channel that communicates over a serial connection.
    """

    def __init__(
        self, port: str, baud: int, timeout: float, logger: logging.Logger = None
    ):
        """
        Initialise serial.

        :param port: Names the TTY device or COM port to connect to.
        :param baud: The baud rate.
        :param timeout: The timeout, None means reads block until all the data is read or the other side disconnects.
        """
        self._port = port
        self._baud = baud
        self._timeout = timeout
        self._serial = None
        self._logger: logging.Logger = (
            logger if logger else logging.getLogger(str(self))
        )

    def __del__(self):
        self.close()

    @property
    def is_open(self) -> bool:
        return bool(self._serial) and self._serial.is_open

    async def open(self) -> Coroutine[Any, Any, bool]:
        if self._serial:
            self._logger.debug("serial port already open")
            return True
        self._serial = AioSerial(
            self._port,
            baudrate=self._baud,
            timeout=self._timeout,
            rtscts=True,
            dsrdtr=True,
        )
        if self._serial:
            self._logger.debug("serial port opened")
            return True
        self._logger.error("failed to open serial port")
        return False

    async def close_async(self) -> Coroutine[Any, Any, None]:
        self.close()

    def close(self):
        if self._serial:
            self._serial.close()
            self._serial = None

    def read(self, bs=-1):
        return self._serial.read(bs)

    def readline(self):
        return self._serial.readline()

    def write(self, data):
        return self._serial.write(data)

    async def read_async(self, bs=-1):
        return await self._serial.read_async(bs)

    async def readline_async(self):
        return await self._serial.readline_async()

    async def write_async(self, data):
        return await self._serial.write_async(data)

    def __repr__(self):
        return f"{self._port} at {self._baud} Bd"
