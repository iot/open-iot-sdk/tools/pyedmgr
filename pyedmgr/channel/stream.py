# Copyright (c) 2022, Arm Limited and Contributors. All rights reserved.
# SPDX-License-Identifier: Apache-2.0
"""
Stream channels.
"""

from functools import partial
from typing import Any, BinaryIO, Coroutine

from .abstract import AbstractChannel
from ..internal import asyncify

__all__ = ["StreamChannel"]

READLINE_BUF_SIZE = 128


class StreamChannel(AbstractChannel):
    """
    Channel that communicates over a binary stream.
    """

    def __init__(self, stream: BinaryIO, name: str = None):
        """
        Initialise the channel.

        :param stream: A binary stream.
        :param name: A friendly name for the channel.
        """
        if not hasattr(stream, "read") or not hasattr(stream, "write"):
            raise TypeError("stream does not implement read/write")

        self._stream = stream
        self._name = name

        # If the stream-like provides a readline implementation, use it as it may be optimized or provide more features.
        # Otherwise, if it provides readuntil, use it to implement readline.
        # Otherwise, use a custom implementation.
        if hasattr(stream, "readline"):
            self._readline_impl = self._stream.readline
        elif hasattr(stream, "readuntil"):
            self._readline_impl = partial(self._stream.readuntil, b"\n")
        else:
            self._readline_buf = b""
            self._readline_impl = self._readline_custom_impl

    def __del__(self):
        self.close()

    @property
    def is_open(self) -> bool:
        return bool(self._stream)

    async def open(self) -> Coroutine[Any, Any, bool]:
        if self._stream:
            return True
        raise TypeError("Stream cannot be reopened once closed")

    def read(self, bs=-1):
        return self._stream.read(bs)

    def write(self, data):
        return self._stream.write(data)

    def readline(self):
        return self._readline_impl()

    def close(self) -> None:
        """
        Close the underlying stream.
        """
        if hasattr(self._stream, "close"):
            self._stream.close()
            self._stream = None

    async def read_async(self, bs=-1):
        return await asyncify(self.read, bs)

    async def readline_async(self):
        return await asyncify(self.readline)

    async def write_async(self, data):
        await asyncify(self.write, data)

    async def close_async(self) -> Coroutine[Any, Any, None]:
        await asyncify(self.close)

    def __repr__(self):
        if self._name:
            return self._name
        return "<anon stream>"

    def _readline_custom_impl(self):
        buf = self._readline_buf
        self._readline_buf = b""
        while True:
            elems = buf.split(b"\n")
            if len(elems) > 1:
                self._readline_buf = b"\n".join(elems[1:])
                return elems[0] + b"\n"
            rdbuf = self._stream.read(READLINE_BUF_SIZE)
            if rdbuf:
                buf += rdbuf
            else:
                return buf
