# Copyright (c) 2022, Arm Limited and Contributors. All rights reserved.
# SPDX-License-Identifier: Apache-2.0

"""
AbstractChannel
"""

from abc import ABC, abstractmethod
from typing import Any, Coroutine

from ..internal import syncify


class AbstractChannel(ABC):
    """
    Base class for Device communication channels.
    """

    @property
    @abstractmethod
    def is_open(self) -> bool:
        """
        Indicates whether the channel is open.
        """
        raise NotImplementedError()

    @abstractmethod
    async def open(self) -> Coroutine[Any, Any, bool]:
        """
        Open the channel asynchronously.
        :return: True if the channel opened successfully, otherwise False.
        """
        raise NotImplementedError()

    def close(self) -> None:
        """
        Close the channel synchronously.
        """
        syncify(self.close_async)

    @abstractmethod
    async def close_async(self) -> Coroutine[Any, Any, None]:
        """
        Close the channel asynchronously.
        """
        raise NotImplementedError()

    @abstractmethod
    def read(self, bs: int = -1) -> bytes:
        """
        Read bytes.

        :param bs: The maximum number of bytes to read if > 0, otherwise read until the channel is closed by the other
        side.

        :return: The bytes read.
        """
        raise NotImplementedError()

    @abstractmethod
    def readline(self) -> bytes:
        """
        Read until b'\n' is reached.

        :return: The bytes read up to and including b'\n'. If there is no b'\n' then the channel was closed during the
        read.
        """
        raise NotImplementedError()

    @abstractmethod
    def write(self, data: bytes) -> int:
        """
        Write bytes.

        :param data: The bytes to write.

        :return: The number of bytes written.
        """
        raise NotImplementedError()

    @abstractmethod
    async def read_async(self, bs: int = -1) -> Coroutine[Any, Any, bytes]:
        """
        Asynchronous equivalent of read.
        """
        raise NotImplementedError()

    @abstractmethod
    async def readline_async(self) -> Coroutine[Any, Any, bytes]:
        """
        Asynchronous equivalent of readline.
        """
        raise NotImplementedError()

    @abstractmethod
    async def write_async(self, data: bytes) -> Coroutine[Any, Any, int]:
        """
        Asynchronous equivalent of write.
        """
        raise NotImplementedError()

    @abstractmethod
    def __repr__(self) -> str:
        raise NotImplementedError()

    def __str__(self) -> str:
        who = type(self).__name__
        what = repr(self)
        return f"{who} ({what})"
