# Copyright (c) 2022, Arm Limited and Contributors. All rights reserved.
# SPDX-License-Identifier: Apache-2.0

"""
Socket channel
"""

import asyncio
from asyncio import StreamReader, StreamWriter
import logging
import socket
from typing import Any, Coroutine
import traceback

from pyedmgr.internal.util import asyncify, syncify

from .abstract import AbstractChannel

__all__ = ["SocketChannel", "SynchronousSocketChannel"]


class SocketChannel(AbstractChannel):
    """
    Channel that communicates via a socket.
    """

    def __init__(self, host: str, port: int, logger: logging.Logger = None):
        """
        Initialise the channel.

        :param host: The hostname or IP address.
        :param port: The port.
        """
        self._host: str = host
        self._port: int = port
        self._reader: StreamReader = None
        self._writer: StreamWriter = None
        self._logger: logging.Logger = (
            logger if logger else logging.getLogger(str(self))
        )

    @property
    def is_open(self) -> bool:
        return all((self._reader, self._writer))

    async def open(self) -> Coroutine[Any, Any, bool]:
        if None in (self._reader, self._writer):
            self._reader, self._writer = await asyncio.open_connection(
                self._host, self._port
            )
            if None in (self._reader, self._writer):
                self._logger.error("failed to open connection")
                return False
            self._logger.debug("connection opened successfully")
        return True

    def close(self):
        if bool(self._reader):
            self._reader = None
        if bool(self._writer):
            self._writer.close()
            self._writer = None

    def read(self, bs=-1):
        return syncify(self.read_async, bs)

    def readline(self):
        return syncify(self.readline_async)

    def write(self, data):
        return syncify(self.write_async, data)

    async def read_async(self, bs=-1):
        return await self._reader.read(bs)

    async def readline_async(self):
        return await self._reader.readline()

    async def write_async(self, data):
        self._writer.write(data)
        await self._writer.drain()

    async def close_async(self) -> Coroutine[Any, Any, None]:
        if bool(self._reader):
            self._reader = None
        if bool(self._writer):
            self._writer.close()
            await self._writer.wait_closed()
            self._writer = None

    def __repr__(self):
        return f"{self._host}:{self._port}"


class SynchronousSocketChannel(AbstractChannel):
    """
    Channel that communicates via a socket.
    """

    def __init__(self, host: str, port: int, logger: logging.Logger = None):
        """
        Initialise the channel.

        :param host: The hostname or IP address.
        :param port: The port.
        """
        self._host: str = host
        self._port: int = port
        self._socket: socket.socket = None
        self._logger: logging.Logger = (
            logger if logger else logging.getLogger(str(self))
        )

    @property
    def is_open(self) -> bool:
        return bool(self._socket)

    def open(self) -> bool:
        if self._socket:
            return True

        try:
            self._socket = socket.create_connection((self._host, self._port))
        except Exception as e:
            self._logger.debug(
                f"{e}:\n" + "".join(traceback.format_tb(e.__traceback__))
            )
            self._logger.error(f"{e}")
            self._socket = None
        else:
            return True

    def close(self):
        if bool(self._socket):
            self._socket.close()
            self._socket = None

    def read(self, bs=-1):
        buf = b""
        while bs < 0 or len(buf) < bs:
            buf += self._socket.recv(1)
        return buf

    def readline(self):
        buf = b""
        while True:
            b = self._socket.recv(1)
            if b == b"\n":
                break
            buf += b
        return buf

    def write(self, data):
        self._socket.send(data)

    def read_async(self, bs=-1):
        return asyncify(self.read, bs)

    def readline_async(self):
        return asyncify(self.readline)

    def write_async(self, data):
        return asyncify(self.write, data)

    def close_async(self):
        return asyncify(self.close)

    def __repr__(self):
        return f"{self._host}:{self._port}"
