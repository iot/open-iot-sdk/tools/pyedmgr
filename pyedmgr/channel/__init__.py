# Copyright (c) 2022, Arm Limited and Contributors. All rights reserved.
# SPDX-License-Identifier: Apache-2.0

"""
Communication channels.
"""

from .abstract import *
from .serial import *
from .socket import *
from .stream import *
