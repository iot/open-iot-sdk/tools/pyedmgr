# Copyright (c) 2022, Arm Limited and Contributors. All rights reserved.
# SPDX-License-Identifier: Apache-2.0

"""
Device.
"""

from functools import partial
from typing import Optional, List

from pyedmgr.internal.util import envify

from .abstract import AbstractDevice
from .device import Device
from ..channel import SerialChannel, SocketChannel
from ..controller import (
    MbedController,
    FvpController,
    SerialServerController,
    SocketServerController,
)
from ..firmware import FirmwareType
from ..flasher import MbedFlasher, NullFlasher
from ..internal import DEFAULT_BAUD, DEFAULT_HOST, DEFAULT_PORT, DEFAULT_TIMEOUT


class DeviceFactory:
    """
    Factory methods for Device.
    """

    def fake_serial(self, baud: int = None, timeout: float = None) -> AbstractDevice:
        """
        Construct a fake serial device.
        """
        timeout = DEFAULT_TIMEOUT if timeout is None else timeout
        baud = DEFAULT_BAUD if baud is None else baud
        server = SerialServerController(baud)
        return Device(
            "Fake Serial Device",
            server,
            partial(SerialChannel, server.port, baud, timeout),
            NullFlasher,
        )

    def fake_socket(self, host: str = None, port: int = None) -> AbstractDevice:
        """
        Construct a fake socket device.
        """
        host = DEFAULT_HOST if host is None else host
        port = DEFAULT_PORT if port is None else port
        server = SocketServerController(host, port)
        return Device(
            "Fake Socket Device",
            server,
            partial(SocketChannel, host, port),
            NullFlasher,
        )

    # pylint:disable=invalid-name,unused-argument
    def load_fvp(
        self,
        name: str,
        config: Optional[str] = None,
        MPS3: Optional[str] = None,
        MPS2: Optional[str] = None,
        args: Optional[List[str]] = None,
        **kwargs,
    ) -> AbstractDevice:
        """
        Construct an FVP device.
        """
        if not config:
            if MPS3:
                config = MPS3
            elif MPS2:
                config = MPS2
        if config:
            config = envify(config)
        ctrl = FvpController(name, config=config, args=args)
        return Device(name, ctrl, ctrl.get_channel, ctrl.get_flasher, FirmwareType.ELF)

    # pylint:enable=invalid-name,unused-argument

    def fvp_cs300_u55(self, mps3="MPS3.conf") -> AbstractDevice:
        """
        Shorthand for load_fvp('FVP_Corstone_SSE-300_Ethos-U55', 'MPS3.conf')
        """
        return self.load_fvp("FVP_Corstone_SSE-300_Ethos-U55", MPS3=mps3)

    def fvp_cs300_u65(self, mps3="MPS3.conf") -> AbstractDevice:
        """
        Shorthand for load_fvp('FVP_Corstone_SSE-300_Ethos-U65', 'MPS3.conf')
        """
        return self.load_fvp("FVP_Corstone_SSE-300_Ethos-U65", MPS3=mps3)

    # pylint:disable=W0613
    def load_mbed(
        self,
        serial_port: str = None,
        baud: int = None,
        timeout: float = None,
        vendor_id: str = None,
        product_id: str = None,
        device_type: str = None,
        platform_name: str = None,
        target_id: str = None,
        **kwargs,
    ) -> AbstractDevice:
        """
        Construct an Mbed device.
        """
        name = f"{device_type} {platform_name} ({vendor_id}:{product_id})"
        baud = baud if baud else DEFAULT_BAUD
        timeout = timeout if timeout else DEFAULT_TIMEOUT
        allowed_firmware_types = 0
        if "nrf" in name.lower():
            allowed_firmware_types |= FirmwareType.HEX
        else:
            allowed_firmware_types |= FirmwareType.BINARY
        return Device(
            name,
            MbedController(serial_port, target_id),
            partial(SerialChannel, serial_port, baud, timeout),
            partial(MbedFlasher, target_id),
            allowed_firmware_types,
        )

    # pylint:enable=W0613
