# Copyright (c) 2022, Arm Limited and Contributors. All rights reserved.
# SPDX-License-Identifier: Apache-2.0

"""
Device.
"""

import logging
from typing import Any, Callable, Coroutine, List, Tuple

from .abstract import AbstractDevice
from ..channel import AbstractChannel
from ..controller import AbstractController, FvpController
from ..firmware import Firmware, FirmwareType
from ..flasher import AbstractFlasher


class Device(AbstractDevice):
    """
    Represents a device.
    """

    def __init__(
        self,
        name: str,
        controller: AbstractController,
        channel_factory: Callable[[], AbstractChannel],
        flasher_factory: Callable[[], AbstractFlasher],
        *allowed_firmware_types: FirmwareType,
        logger: logging.Logger = None,
    ):
        """
        Initialise device (internal use).
        :param name: A friendly name for the device.
        :param controller: The controller.
        :param channel_factory: () -> AbstractChannel
        :param flasher_factory: () -> AbstractFlasher
        :param allowed_firmware_types: Optional list of firmware types that can be flashed to the device. Defaults to
        BINARY if none are provided.
        :param logger: Optional logger.
        """
        self._name = name
        self._controller = controller
        self._channel_factory = channel_factory
        self._flasher_factory = flasher_factory
        self._channel: AbstractChannel = None
        self._flasher: AbstractFlasher = None
        self._logger = logger if logger else logging.getLogger(name)
        if allowed_firmware_types:
            self._allowed_firmware_types = allowed_firmware_types
        else:
            self._allowed_firmware_types = (
                FirmwareType.BINARY | FirmwareType.ELF | FirmwareType.HEX
            )

    @property
    def name(self) -> str:
        return self._name

    @property
    def allowed_firmware_types(self) -> FirmwareType:
        return self._allowed_firmware_types

    @property
    def connected(self) -> bool:
        return bool(self._channel) and self._channel.is_open

    @property
    def channel(self) -> AbstractChannel:
        return self._channel

    @property
    def controller(self) -> AbstractController:
        return self._controller

    @property
    def clonable(self) -> bool:
        return self._controller.clonable

    async def connect(self) -> Coroutine[Any, Any, None]:
        await self._controller.start()

        if not self._channel:
            self._channel = self._channel_factory()
        if not self._channel.is_open:
            await self._channel.open()

    async def disconnect(self, shutdown=False) -> Coroutine[Any, Any, None]:
        if self._channel:
            if self._channel.is_open:
                await self._channel.close_async()
            self._channel = None

        if shutdown:
            await self._controller.shutdown()
        else:
            await self._controller.pause()

    async def flash(
        self, firmware: Firmware, additional_files: List[Tuple[str, int]] = None
    ) -> Coroutine[Any, Any, None]:
        await self.controller.pause()
        async with self._flasher_factory() as session:
            session.firmware_list.append(firmware)
            if additional_files:
                session.firmware_list.extend(additional_files)
            await session.transfer_everything()
        await self.controller.start()

    async def reset(self) -> Coroutine[Any, Any, None]:
        await self._controller.reset()

    async def clone(self, **kwargs) -> Coroutine[Any, Any, AbstractDevice]:
        ctrl = await self.controller.clone(**kwargs)
        channel_factory = self._channel_factory
        flasher_factory = self._flasher_factory
        if isinstance(ctrl, FvpController):
            channel_factory = ctrl.get_channel
            flasher_factory = ctrl.get_flasher
        clone = Device(
            self._name,
            ctrl,
            channel_factory,
            flasher_factory,
            *self._allowed_firmware_types,
        )
        if self.connected:
            await clone.connect()
        return clone

    def __str__(self) -> str:
        return f"{self.name} <channel={self.channel}>"
