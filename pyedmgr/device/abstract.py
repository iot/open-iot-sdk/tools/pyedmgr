# Copyright (c) 2022, Arm Limited and Contributors. All rights reserved.
# SPDX-License-Identifier: Apache-2.0

"""
Device.
"""

from abc import ABC, abstractmethod
from typing import Any, Coroutine, List, Tuple, Union

from ..channel import AbstractChannel
from ..controller import AbstractController
from ..firmware import Firmware, FirmwareType


class AbstractDevice:
    ...


class AbstractDevice(ABC):
    """
    Interface implemented by devices.
    """

    @property
    @abstractmethod
    def name(self) -> str:
        """
        Gets a friendly name for the device.
        """
        raise NotImplementedError()

    @property
    @abstractmethod
    def allowed_firmware_types(self) -> FirmwareType:
        """
        Indicates whether the device requires firmware to have a symbol table or be a flat ELF file.
        """
        raise NotImplementedError()

    @property
    @abstractmethod
    def connected(self) -> bool:
        """
        Indicates whether the device is connected.
        """
        raise NotImplementedError()

    @property
    @abstractmethod
    def channel(self) -> AbstractChannel:
        """
        Gets the communication channel.
        """
        raise NotImplementedError()

    @property
    @abstractmethod
    def controller(self) -> AbstractController:
        """
        Gets the controller.
        """
        raise NotImplementedError()

    @property
    @abstractmethod
    def clonable(self) -> bool:
        """
        Indicates whether the device can be cloned.
        """
        raise NotImplementedError()

    @abstractmethod
    async def connect(self) -> Coroutine[Any, Any, None]:
        """
        Connects to the device.
        """
        raise NotImplementedError()

    @abstractmethod
    async def disconnect(self, shutdown=False) -> Coroutine[Any, Any, None]:
        """
        Disconnects from the device. If shutdown is set, the device is additionally shut down.
        """
        raise NotImplementedError()

    @abstractmethod
    async def flash(
        self, firmware: Firmware, additional_files: List[Tuple[str, int]] = None
    ) -> Coroutine[Any, Any, None]:
        """
        Flash a binary to the device.
        :param firmware: Names a file containing firmware.
        :param additional_files: Names any additional files and the memory addresses to load them at.
        """
        raise NotImplementedError()

    @abstractmethod
    async def reset(self) -> Coroutine[Any, Any, None]:
        """
        Reset the device.
        """
        raise NotImplementedError()

    @abstractmethod
    async def clone(self) -> AbstractDevice:
        """
        Clone the device. If the device was an FVP and was started and connected the clone will also be started (with
        its own controller) and connected (with its own channel), but not flashed.
        :raises: NotImplementedError for boards.
        """
        raise NotImplementedError()

    def can_flash_with(self, firmware: Union[Firmware, FirmwareType]) -> bool:
        """
        Indicates whether the device can be flashed with the given firmware or firmware type.
        """
        firmware_type = firmware
        if isinstance(firmware, Firmware):
            firmware_type = firmware.type
        assert isinstance(firmware_type, FirmwareType)

        return firmware_type in self.allowed_firmware_types
