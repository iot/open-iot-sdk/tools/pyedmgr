# Copyright (c) 2022, Arm Limited and Contributors. All rights reserved.
# SPDX-License-Identifier: Apache-2.0

"""
SocketServerController.
"""

import asyncio
import os
import socket
from typing import Any, Coroutine

from .abstract import AbstractController
from ..internal import syncify, ensure_done

__all__ = ["SocketServerController"]


async def _accept(reader, writer):
    print("Socket server accepted connection")
    bs = await reader.readline()
    if bs:
        writer.write(bs)
        await writer.drain()
    writer.close()
    await writer.wait_closed()


class SocketServerController(AbstractController):
    """
    Controls a server that serves over a socket.
    """

    def __init__(self, host: str, port: int):
        self._host = host
        self._port = port
        self._server: asyncio.AbstractServer = None
        self._task: asyncio.Task = None

    def __del__(self):
        self._shutdown()

    async def start(self):
        if self._server:
            return

        # Force IPv4 when running inside Docker
        family = socket.AF_UNSPEC
        if os.path.exists("/.dockerenv"):
            family = socket.AF_INET

        self._server = await asyncio.start_server(
            _accept, self._host, self._port, family=family, start_serving=True
        )
        self._task = asyncio.create_task(self._server.serve_forever())
        print("Socket server listening")

    async def pause(self) -> Coroutine[Any, Any, None]:
        return NotImplemented

    async def shutdown(self) -> Coroutine[Any, Any, None]:
        if not self._server:
            return
        self._server.close()
        if isinstance(self._task, asyncio.Task) and self._task.get_loop().is_running():
            await ensure_done(self._task)
        await self._server.wait_closed()
        self._server = None

    async def reset(self):
        return NotImplemented

    def __str__(self):
        status = "Running" if self._server else "Stopped"
        return f"Socket server ({self._host}:{self._port}) [{status}]"

    def _shutdown(self):
        syncify(self.shutdown)
