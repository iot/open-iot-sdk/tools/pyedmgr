# Copyright (c) 2022, Arm Limited and Contributors. All rights reserved.
# SPDX-License-Identifier: Apache-2.0

"""
SerialServerController.
"""

import asyncio
import logging
import os
import pty
import select
import threading
import time
from typing import Any, Coroutine

from .abstract import AbstractController
from ..internal import DEFAULT_BAUD, asyncify

__all__ = ["SerialServerController"]


class SerialServerController(AbstractController):
    """
    Controls a server that serves over virtual serial port.
    """

    def __init__(self, baud: int = DEFAULT_BAUD, logger: logging.Logger = None):
        self._baud = baud
        self._thread = None
        self._primary_fd, self._secondary_fd = pty.openpty()
        self._port = os.ttyname(self._secondary_fd)
        self._logger = logger if logger else logging.getLogger(str(self))
        self._lock = threading.Lock()
        self._running = False

    def __del__(self):
        self._shutdown()

    @property
    def port(self) -> int:
        """
        Gets the port.
        """
        return self._port

    @property
    def baud(self) -> int:
        """
        Gets the baud rate.
        """
        return self._baud

    def _run_async(self):
        while self._running:
            # Since we can't use O_NONBLOCKING or setblocking(False) with PTY and the only way to wake up a thread
            # blocked on read() is to trigger an exception (which is complicated to do from another thread) we use
            # select to check if the FD is ready for reading. If not, we check the condition again.
            rlist, _, _ = select.select(
                [self._primary_fd], [], [], 1e-6
            )  # Timeout of 1 us. We really want 0 timeout
            # but the API doesn't allow it.
            if self._primary_fd not in rlist:
                time.sleep(0)
                continue
            bs = os.read(self._primary_fd, 4096)
            os.write(self._primary_fd, bs)

    async def start(self) -> Coroutine[Any, Any, asyncio.Task]:
        self._running = True
        self._thread = threading.Thread(target=self._run_async)
        self._thread.start()

    async def pause(self) -> Coroutine[Any, Any, None]:
        return NotImplemented

    async def shutdown(self) -> Coroutine[Any, Any, None]:
        await asyncify(self._shutdown)

    def _shutdown(self):
        if not self._thread:
            return
        with self._lock:
            # Signal thread to shut down
            self._logger.debug("Shutting down")
            self._running = False
            self._thread.join()
            # Clean up
            if self._primary_fd is not None:
                os.close(self._primary_fd)
                self._primary_fd = None
            if self._primary_fd is not None:
                os.close(self._secondary_fd)
                self._secondary_fd = None
            self._logger.debug("Shut down")
            self._thread = None

    async def reset(self):
        return NotImplemented

    def __str__(self):
        return f"{type(self).__name__} ({self._port})"
