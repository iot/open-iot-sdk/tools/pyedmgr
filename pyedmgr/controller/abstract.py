# Copyright (c) 2022, Arm Limited and Contributors. All rights reserved.
# SPDX-License-Identifier: Apache-2.0

"""
AbstractController.
"""

from abc import ABC, abstractmethod


class AbstractController(ABC):
    """
    Interface implemented by controllers.
    """

    @abstractmethod
    async def start(self):
        """
        Start the device; start could mean boot or resume.
        """
        raise NotImplementedError()

    @abstractmethod
    async def pause(self):
        """
        Pause execution of the controlled device.
        :return: NotImplemented if not supported.
        """
        raise NotImplementedError()

    @abstractmethod
    async def shutdown(self):
        """
        Shut down the controlled device.
        :return: NotImplemented if not supported.
        """
        raise NotImplementedError()

    @abstractmethod
    async def reset(self):
        """
        Reset the device. Execution continues from PC=0.
        :return: NotImplemented if not supported.
        """
        raise NotImplementedError()

    @property
    def clonable(self):
        """
        Indicates whether the controlled device can be cloned.
        """
        return False

    async def clone(self, **kwargs):
        """
        Clones the controller. If the controller was started the clone will also be started, but not flashed.
        :raises: NotImplementedError if the controller cannot be cloned.
        :return: The cloned controller.
        """

        raise NotImplementedError()
