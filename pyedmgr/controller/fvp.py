# Copyright (c) 2022, Arm Limited and Contributors. All rights reserved.
# SPDX-License-Identifier: Apache-2.0

"""
FvpController
"""

import atexit
from collections import defaultdict
import logging
import os
import shutil
from subprocess import Popen, PIPE, STDOUT
from threading import Lock, Thread
import threading
import time
from typing import Any, List, Optional
from pyedmgr.channel.socket import SynchronousSocketChannel

try:
    from iris.debug.Model import NetworkModel
    try:
        from iris.debug.Target import SyncTarget as IrisTarget
    except:
        from iris.debug.Target import Target as IrisTarget
    import iris.error
except ImportError:
    logging.warning('Could not import Iris, controlling FVPs will not be possible')
    NetworkModel = Any
    IrisTarget = Any

from .abstract import AbstractController
from ..channel import AbstractChannel, SocketChannel
from ..flasher import AbstractFlasher, IrisFlasher
from ..internal import asyncify

__all__ = ["FvpController"]


class FvpController:
    ...


class FvpController(AbstractController):
    """
    Control an FVP/fastmodel."""

    _lock = Lock()
    _clones = defaultdict(list)

    def __init__(self, name: str, config: str = None, args: Optional[List[str]] = None):
        """
        Initialise FVP.

        :param name: The fast model name. Must be an executable binary.
        :param config: Names a config file (usually MPS2.conf or MPS3.conf) for the fastmodel.
        """
        self._name: str = name
        self._model: NetworkModel = None
        self._process: Popen = None
        self._config: str = config
        self._args: Optional[List[str]] = args

        self._port: List[int] = [None] * 6

        self._clear_output_thread: Thread = None
        self._clear_output_thread_stop_event: threading.Event = None

        atexit.register(self.__del__)

        with self._lock:
            self._clones[name].append(self)
            id_ = self._clones[name].index(self)
            self._logger = logging.getLogger(f"{name}[{id_}]")

    def __del__(self):
        try:
            self._shutdown_sync()
        except iris.error.IrisError:
            # Raised when we issue the shutdown command but the FVP had already exited due to an error.
            pass
        finally:
            atexit.unregister(self.__del__)

    @property
    def telnet_port(self) -> List[Optional[int]]:
        return list(self._port)

    def get_channel(self, terminal=0, sync=False) -> AbstractChannel:
        """
        Instantiate and return a channel that communicates with the FVP.
        """
        host = self._model.client.hostname

        # If the port for the terminal was logged then the logged port will be correct. Otherwise we have to guess based
        # on the fm_agent value which is itself a guess.
        port = self._port[terminal]

        if sync:
            return SynchronousSocketChannel(host, port)
        return SocketChannel(host, port)

    def get_flasher(self) -> AbstractFlasher:
        """
        Instantiate and return flasher.
        """
        return IrisFlasher(self._cpu)

    async def start(self):
        await asyncify(self._start_sync)

    async def pause(self):
        await asyncify(self._pause_sync)

    async def shutdown(self):
        await asyncify(self._shutdown_sync)

    async def reset(self):
        if not self._model:
            raise ValueError("Model was not started")
        await self.pause()
        cpu = self._cpu
        pc = cpu.pc_name_prefix + cpu.pc_info.name
        cpu.write_register(pc, 0)
        await self.resume()

    @property
    def clonable(self) -> bool:
        return True

    async def clone(self, **kwargs):
        clone = FvpController(
            kwargs.get("name", self._name), config=kwargs.get("config", self._config)
        )

        if self._cpu.is_running:
            await clone.start()

        return clone

    def _shutdown_sync(self):
        try:
            if self._model:
                self._model.release(shutdown=True)
                del self._model
                self._model = None
                self._logger.debug("Model released")
        finally:
            if self._clear_output_thread:
                self._clear_output_thread_stop_event.set()
                self._clear_output_thread.join()
                self._clear_output_thread = None
                self._clear_output_thread_stop_event = None

            if self._process:
                self._process.terminate()
                self._process.wait()
                del self._process
                self._process = None
                self._logger.debug("Process terminated")

    def _spawn(self):
        full_path = shutil.which(self._name)
        if not full_path:
            raise RuntimeError(f'Could not find binary: {self._name}')

        args = [
            full_path, # Use fully-qualified path
            "-I",      # Start IRIS server
            "-p",      # Print telnet ports
        ]
        if self._config:
            args.extend(("-f", self._config))

        if self._args:
            args.extend(self._args)

        self._logger.info("Spawn: " + " ".join(args))

        self._process = Popen(args, stdin=PIPE, stdout=PIPE, stderr=STDOUT)

        if self._process.pid is None:
            raise RuntimeError("Failed to spawn")

        iris_port = None
        have_telnet_port = False
        for line in self._process.stdout:
            words = line.decode("utf-8", "replace").split()
            if not words:
                # Stop on empty line.
                break

            if words[0].startswith("telnetterminal"):
                which = int(words[0][-2])
                self._port[which] = int(words[-1])
                have_telnet_port = True
            elif words[0].startswith("Iris"):
                iris_port = int(words[-1])
        if iris_port is None or not have_telnet_port:
            raise RuntimeError("Could not get Iris/telnet port from FVP")

        self._model = NetworkModel(host="localhost", port=iris_port)

        self._logger.debug("Spawned FVP")

        os.set_blocking(self._process.stdout.fileno(), False)

        self._clear_output_thread_stop_event = threading.Event()
        self._clear_output_thread = Thread(target=self._clear_output, name=f'Consume {self._name} output')
        self._clear_output_thread.daemon = True
        self._clear_output_thread.start()

    def _resume(self):
        self._model.run(blocking=False)

    def _start_sync(self):
        if self._model:
            self._resume()
        else:
            self._spawn()

    def _pause_sync(self):
        if self._model:
            self._model.stop()
        self._logger.debug("Model paused")

    @property
    def _cpu(self) -> IrisTarget:
        return self._cpus[0]

    @property
    def _cpus(self) -> List[IrisTarget]:
        return self._model.get_cpus()

    def _log_cpu(self, i, cpu):
        self._logger.debug(
            "cpu{}: pc=0x{:x}, state={}, running={}".format(
                i,
                cpu.read_register(cpu.pc_name_prefix + cpu.pc_info.name),
                cpu.get_execution_state(),
                cpu.is_running,
            )
        )

    def _clear_output(self):
        while not self._clear_output_thread_stop_event.is_set():
            line = self._process.stdout.readline()
            # If nothing is beeing received, sleep for a small amount of time
            if line == b"":
                time.sleep(0.01)
