# Copyright (c) 2022, Arm Limited and Contributors. All rights reserved.
# SPDX-License-Identifier: Apache-2.0

"""
MbedController.
"""

from pyocd.core.helpers import ConnectHelper
from pyocd.core.core_target import Target

from .abstract import AbstractController
from ..internal import asyncify

__all__ = ["MbedController"]


class MbedController(AbstractController):
    """
    Control mbed device.
    """

    def __init__(self, serial_port, unique_id):
        if not isinstance(serial_port, str) or not serial_port:
            raise ValueError("serial_port must be a non-empty string")
        if not isinstance(unique_id, str) or not unique_id:
            raise ValueError("unique_id must be a non-empty string")
        self._serial_port = serial_port
        self._halted = False
        self._unique_id = unique_id
        self._probe = None

    async def start(self):
        if not self._halted:
            return
        await self._ensure_probe()

        with self._probe as session:
            target: Target = session.board.target
            target.resume()
            self._halted = False

    async def pause(self):
        if self._halted:
            return
        await self._ensure_probe()
        with self._probe as session:
            target: Target = session.board.target
            target.halt()
            self._halted = True

    async def shutdown(self):
        return NotImplemented

    async def reset(self):
        await self._ensure_probe()
        with self._probe as session:
            target: Target = session.board.target
            target.reset()

    async def _ensure_probe(self):
        if self._probe:
            return
        self._probe = await asyncify(
            ConnectHelper.session_with_chosen_probe, unique_id=self._unique_id
        )
