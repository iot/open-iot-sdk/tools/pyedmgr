# Copyright (c) 2022, Arm Limited and Contributors. All rights reserved.
# SPDX-License-Identifier: Apache-2.0
"""
MbedFlasher.
        """
import logging
import time
from typing import Any, Coroutine

from mbed_flasher.flash import Flash

from .abstract import AbstractFlashSession, AbstractFlasher, FirmwareBlockContext
from ..internal import asyncify

__all__ = ["MbedFlasherError", "MbedFlasher"]


class MbedFlasherError(RuntimeError):
    """
    Mbed flasher error.
    """


class _MbedFlashSession(AbstractFlashSession):
    def __init__(self, target_id: str):
        super().__init__()
        self._target_id = target_id
        self._flasher = Flash()
        self._logger = logging.getLogger("MbedFlasher")

    # Setting preferred_block_size to -1 and _auto_load_blocks to False results in a Firmware appearing as a single
    # large block with no I/O being performed by the AsyncFirmwareBlocksIterator.
    @property
    def preferred_block_size(self) -> int:
        return -1

    @property
    def _auto_load_blocks(self) -> bool:
        return False

    async def send_file_block(
        self,
        block: FirmwareBlockContext,
        start_time: int = None,
    ) -> Coroutine[Any, Any, Any]:
        if not block.firmware:
            self._logger.warning(f"Ignoring non-firmware file {block.filename}")
            return
        self._logger.debug(f"Flashing {block.filename} to {self._target_id}")
        start_time = time.time()
        await asyncify(
            self._flasher.flash,
            build=block.filename,
            target_id=self._target_id,
            method="simple",
        )
        elapsed_ms = 1000 * (time.time() - start_time)
        block.get_stats(elapsed_ms).log_to(self._logger)


class MbedFlasher(AbstractFlasher):
    """
    Flash firmware to a device via mbed_flasher."""

    def __init__(self, target_id: str):
        """
        Initialise MbedFlasher.
        :param target_id: The target device ID.
        """
        if not isinstance(target_id, str) or not target_id:
            raise ValueError("target_id must be a non-empty string")
        self._target_id: str = target_id
        self._session: _MbedFlashSession = None

    async def __aenter__(self) -> AbstractFlashSession:
        """
        Create a flashing session.
        """
        self._session = _MbedFlashSession(self._target_id)
        return self._session

    async def __aexit__(self, ex_type, ex, tb):
        self._session = None
