# Copyright (c) 2022, Arm Limited and Contributors. All rights reserved.
# SPDX-License-Identifier: Apache-2.0

"""
GdbFlasher.
"""

import logging
import time
from typing import Any, Coroutine, Union

from pygdbmi.gdbcontroller import GdbController


from .abstract import AbstractFlashSession, AbstractFlasher, FirmwareBlockContext
from ..firmware import Firmware
from ..internal import asyncify

__all__ = ["GdbError", "GdbFlasher"]


class GdbError(RuntimeError):
    """
    Exception raised when there is an error from GDB.
    """


class _RemoteGdbController:
    """
    Control remote GDB instance.
    """

    def __init__(self, *args, logger=None):
        """
        Initialise.
        :param host: Remote target host.
        :param port: Remote target port.
        :param args: Arguments for GDB.
        :param logger: Optional logger.
        """
        self._logger = logger if logger else logging.getLogger("gdb")
        self._gdb = self._start_gdb(args)

    async def set_architecture(self, arch):
        """
        Set architecture.
        """
        await self._send_gdb(f"set architecture {arch}")

    async def set_remote_target(self, host, port):
        """
        Set target to remote host:port.
        """
        await self._send_gdb(f"target remote {host}:{port}")

    async def write_memory(self, address, buffer):
        """
        Write 'buffer' into memory at 'address'.
        """
        num_bytes = len(buffer)
        buffer = ",".join(buffer)
        await self._send_gdb(
            f"set ({{uint8_t[{num_bytes}]}}0x{address:x}) = {{{buffer}}}"
        )

    async def load_symbol_table(self, filename):
        """
        Load a symbol table.
        """
        await self._send_gdb(f"file {filename}")

    async def load_file(self, filename, offset=0):
        """
        Load a file into memory.
        """
        await self._send_gdb(f"load {filename} {offset}")

    async def jump(self, address: Union[str, int]):
        """
        Jump to an address or symbol.
        """
        if isinstance(address, int):
            address = f"*0x{address:x}"
        await self._send_gdb(f"jump {address}")

    def _start_gdb(self, args):
        self._logger.debug("Starting GDB: {}".format(" ".join(args)))
        return GdbController(args)

    async def _send_gdb(self, *args):
        res = await asyncify(self._gdb.write, *args)
        err = None
        if isinstance(res, list):
            for obj in res:
                if obj["message"] == "error":
                    err = obj["payload"]
        else:
            if res["message"] == "error":
                err = res["payload"]
        if err:
            if isinstance(err, dict):
                err = err["msg"]
            raise GdbError(err)


class _GdbFlashSession(AbstractFlashSession):
    def __init__(self, host: str, port: int, logger: logging.Logger):
        super().__init__()
        self._host = host
        self._port = port
        self._have_written_firmware = False
        self._gdb = _RemoteGdbController("gdb-multiarch", "--interpreter=mi3", "-q")
        self._logger = logger

    @property
    def _auto_load_blocks(self) -> bool:
        return False

    async def send_file_block(
        self,
        block: FirmwareBlockContext,
        start_time: int = None,
    ) -> Coroutine[Any, Any, Any]:
        if block.firmware:
            return

        address = block.file_address + block.block_index * block.block_size
        block_start_time = time.time()

        await self._gdb.load_file(block.filename, address)

        block_elapsed = time.time() - block_start_time
        file_elapsed = (time.time() - start_time) if start_time else None
        file_written = address - block.file_address

        self._logger.debug(
            (
                "File {}/{} ({}): Block {}/{}: Wrote {} B in {:.1f} s ({:.1f} kB/s) [block]; "
                + "{:.1f}/{:.1f} kB in {:.1f} s ({:.1f} kB/s) [file]"
            ).format(
                block.file_index,
                len(self.firmware_list),
                block.filename,
                block.block_index,
                block.block_count,
                len(block.block_bytes),
                block_elapsed,
                len(block.block_bytes) / 1024 / block_elapsed,
                file_written / 1024,
                block.file_size / 1024,
                file_elapsed if file_elapsed else "N/A",
                (file_written / 1024 / file_elapsed) if file_elapsed else "N/A",
            )
        )

    async def init(self):
        """
        Initialise.
        """
        await self._gdb.set_architecture("arm")
        await self._gdb.set_remote_target(self._host, self._port)

    async def fini(self):
        """
        Finalize.
        """
        for firmware in self.firmware_list:
            if isinstance(firmware, Firmware):
                await self._gdb.load_symbol_table(firmware.path)
                await self._gdb.load_file(firmware.path, 0)
                await self._gdb.jump("Reset_Handler")
                return
        self._logger.warning("No firmware was provided during flash session")


class GdbFlasher(AbstractFlasher):
    """
    Flash firmware to a device via remote GDB connection.
    """

    def __init__(self, host: str, port: int, logger: logging.Logger = None):
        """
        Initialise GdbFlasher.
        :param host: GDB remote host.
        :param port: GDB remote port.
        """
        self._host = host
        self._port = port
        self._session = None
        self._logger = logger if logger else logging.getLogger("GdbFlasher")

    async def __aenter__(self) -> AbstractFlashSession:
        """
        Create a flashing session.
        """
        self._session = _GdbFlashSession(self._host, self._port, self._logger)
        await self._session.init()
        return self._session

    async def __aexit__(self, ex_type, ex, tb):
        await self._session.fini()
        self._session = None
