# Copyright (c) 2022, Arm Limited and Contributors. All rights reserved.
# SPDX-License-Identifier: Apache-2.0

"""
AbstractFlasher.
"""

import logging
from abc import ABC, abstractmethod
import os
import time
from typing import Any, BinaryIO, Coroutine, Iterator, Optional, Tuple, Union

import aiofiles

from ..firmware import Firmware, FirmwareList
from ..internal import get_null_logger

__all__ = [
    "AbstractFlasher",
    "AbstractFlashSession",
    "AsyncFirmwareBlocksIterator",
    "FirmwareBlockContext",
    "FirmwareList",
    "FlashStatistics",
]


class FlashStatistics:
    """
    Flash statistics.
    """

    def __init__(self, filename: str, bytes_written: float, elapsed_ms: float):
        self.filename = filename
        self.bytes_written = bytes_written
        self.elapsed_ms = elapsed_ms

    @property
    def kib(self) -> float:
        """
        kiB written.
        """
        return self.bytes_written / 1024

    @property
    def sec(self) -> float:
        """
        Seconds elapsed.
        """
        return self.elapsed_ms / 1000

    @property
    def kbps(self) -> float:
        """
        kiB/s.
        """
        return self.kib / self.sec

    def __str__(self):
        time_val = self.sec
        time_unit = "s"
        if time_val < 0.1:
            time_val = self.elapsed_ms
            time_unit = "ms"

        size_val = self.kib
        size_unit = "k"
        if size_val >= 1024:
            size_val /= 1024
            size_unit = "M"

        speed_val = self.kbps
        speed_unit = "k"
        if speed_val >= 1024:
            speed_val /= 1024
            speed_unit = "M"

        return (
            f"Flashed {self.filename} ({size_val:.1f} {size_unit}iB) "
            + f"in {time_val:.1f} {time_unit} ({speed_val:.1f} {speed_unit}iB/s)"
        )

    def log_to(self, logger: logging.Logger, level=logging.DEBUG):
        """
        Write str(self) to logger.
        """
        logger.log(level, str(self))


# pylint:disable=missing-class-docstring
class FirmwareBlockContext:
    ...


# pylint:enable=missing-class-docstring


class FirmwareBlockContext:
    """
    Context and content of a block in a firmware/file."""

    def __init__(
        self,
        filename_or_firmware: Union[None, Firmware, str] = None,
        index: Union[None, int] = None,
        address: Union[None, int] = None,
        stream: Union[None, BinaryIO] = None,
        preferred_block_size: Union[None, int] = None,
        other: FirmwareBlockContext = None,
    ):
        """
         Initialise.
        :param filename_or_firmware: An instance of Firmware or a filename. Required if 'other' not given.
        :param index: The 0-based index into a FirmwareList. Optional; ignored if 'other' is given.
        :param address: A memory or LBA address to copy the file to. Optional; ignored if 'other' is given.
        :param stream: An open binary stream to filename. Optional; ignored if 'other' is given.
        :param preferred_block_size: If a positive integer is given, read blocks of preferred_block_size bytes. If 0 is
        given, use st_blksize from os.stat(). If a negative value is given, treat the entire file as one block.
        :param other: Another instance to copy (all other params ignored).
        """
        if other:
            self.firmware: Firmware = other.firmware
            self.filename: str = other.filename
            self.file_index: int = other.file_index
            self.file_address: int = other.file_address
            self.stream: BinaryIO = other.stream
            self.file_size: int = other.file_size
            self.block_size: int = other.block_size
            self.block_count: int = other.block_count
            self.block_index: int = other.block_index
            self.block_address: int = other.block_address
            self.block_bytes: bytes = other.block_bytes
            return

        if isinstance(filename_or_firmware, Firmware):
            self.firmware: Firmware = filename_or_firmware
            self.filename: str = filename_or_firmware.path
        elif isinstance(filename_or_firmware, str):
            self.firmware: Firmware = None
            self.filename: str = filename_or_firmware
        else:
            raise TypeError("Invalid type or value given for filename_or_firmware")

        stat = os.stat(self.filename)
        if preferred_block_size == 0:
            preferred_block_size = stat.st_blksize
        elif preferred_block_size < 0:
            preferred_block_size = stat.st_size
        self.file_index: int = index if index else -1
        self.file_address: int = address
        self.stream: BinaryIO = stream
        self.file_size: int = stat.st_size
        self.block_size: int = preferred_block_size
        self.block_count: int = self.file_size // self.block_size + int(
            self.file_size % self.block_size > 0
        )
        self.block_index: int = 0
        self.block_address: int = address
        self.block_bytes: bytes = None

    def clone(self):
        """
        Create a shallow clone of the context.
        """
        return FirmwareBlockContext(other=self)

    def get_stats(self, elapsed_ms: float) -> FlashStatistics:
        """
        Get flash statistics for the block.
        """
        bytes_written = len(self.block_bytes) if self.block_bytes else self.block_size
        return FlashStatistics(self.filename, bytes_written, elapsed_ms)


class AsyncFirmwareBlocksIterator:
    """
    Asynchronously iterate blocks in a firmware or file.
    """

    def __init__(
        self,
        firmware_list: FirmwareList,
        preferred_block_size: int = 0,
        auto_load_blocks: bool = True,
    ):
        """
         Initialise iterator.
        :param firmware_list: A list of firmware and files to iterater.
        :param preferred_block_size: @see FirmwareBlockContext
        :param auto_load_blocks: Whether to automatically read the contents of blocks into memory when they are iterated
        to. If set to false the iterator performs no async I/O and functions as a normal iterator. Optional; ignored if
        'other' is given.
        """
        self._firmware_iter: Iterator[Union[Firmware, Tuple[str, int]]] = iter(
            firmware_list
        )
        self._preferred_block_size = preferred_block_size
        self._auto_load_blocks = auto_load_blocks
        self._current_block: FirmwareBlockContext = None

    async def __anext__(self) -> Coroutine[Any, Any, FirmwareBlockContext]:
        if self._current_block is None:
            await self._next_file()
        else:
            await self._next_block()
        return self._current_block

    async def _next_file(self):
        file_or_firmware = None
        try:
            file_or_firmware = next(self._firmware_iter)
        except StopIteration:
            # pylint:disable=raise-missing-from
            raise StopAsyncIteration
            # pylint:enable=raise-missing-from
        else:
            address = None
            index = (self._current_block.file_index + 1) if self._current_block else 0
            filename = None
            if isinstance(file_or_firmware, tuple):
                filename, address = file_or_firmware
                file_or_firmware = filename
            else:
                filename = file_or_firmware.path
            stream = None
            if self._auto_load_blocks:
                stream = await aiofiles.open(filename, "rb")
            self._current_block = FirmwareBlockContext(
                file_or_firmware, index, address, stream, self._preferred_block_size
            )
            if stream:
                await self._read_block()

    async def _next_block(self):
        if self._current_block.block_index + 1 >= self._current_block.block_count:
            await self._next_file()
            return

        # Make a clone of the block but advance the index.
        self._current_block = self._current_block.clone()
        self._current_block.block_index += 1
        self._current_block.block_bytes = None  # Allow memory to be reclaimed if any
        if self._current_block.block_address is not None:
            self._current_block.block_address += self._current_block.block_size

        # Load the block bytes.
        if self._current_block.stream:
            await self._read_block()

    async def _read_block(self):
        n = self._current_block.block_size
        if n < 0:
            n = self._current_block.file_size
        self._current_block.block_bytes = await self._current_block.stream.read(n)


class AbstractFlashSession(ABC):
    """
    Flashing session.
    """

    def __init__(self):
        self._firmware_list = FirmwareList()

    @property
    def firmware_list(self) -> FirmwareList:
        """
        Gets the list of files and firmware to be transferred. Files and firmware may be added by users.
        """
        return self._firmware_list

    @property
    def preferred_block_size(self) -> int:
        """
         Gets the preferred block-size if any (override).
        <0: transfer entire file.
        0: use the value of st_blksz from os.stat().
        >0: use the returned value.
        """
        return 0

    @property
    def _auto_load_blocks(self) -> bool:
        """
        Whether to automatically load block data.
        """
        return True

    def __aiter__(self) -> AsyncFirmwareBlocksIterator:
        """
        Asynchronously iterate the blocks.
        """
        return AsyncFirmwareBlocksIterator(
            self.firmware_list, self.preferred_block_size, self._auto_load_blocks
        )

    async def transfer_everything(self):
        """
        Iterate the firmware list and transfer all blocks in order. The whole list is transmitted each time this is called so it should usually only be called once.
        """
        logger: Optional[logging.Logger] = None
        num_files = len(self.firmware_list)
        if hasattr(self, "_logger"):
            logger = getattr(self, "_logger")
            logger.debug(f"Starting transfer of {num_files} files")
        else:
            logger = get_null_logger()

        start_time = time.time()
        total_bytes = 0
        num_blocks = 0
        async for block in self:
            await self.send_file_block(block, start_time)
            total_bytes += block.block_size
            num_blocks += 1

        elapsed_ms = (time.time() - start_time) * 1000
        file_s = "" if num_files == 1 else "s"
        block_s = "" if num_blocks == 1 else "s"
        stats = FlashStatistics(
            f"{num_files} file{file_s} ({num_blocks} block{block_s})",
            total_bytes,
            elapsed_ms,
        )
        stats.log_to(logger)

    @abstractmethod
    async def send_file_block(
        self,
        block: FirmwareBlockContext,
        start_time: int = None,
    ) -> Coroutine[Any, Any, Any]:
        """
        Send a block as returned from next(AbstractFlashSession)
        """
        raise NotImplementedError()


class AbstractFlasher(ABC):
    """
    Interface implemented by flashers. Instantiate a flasher for each device.

    :example:
        async with MyFlasher(args) as flasher:
            flasher.firmware_list.append(firmware)
            flasher.firmware_list.append((filename, address))
            await flasher.transfer_everything()

    :example:
        flasher = MyFlasher(args)
        async with flasher as session:
            session.firmware_list.append(firmware)
            session.firmware_list.append((filename, address))
            async for block in session:
                await session.send_file_block(block)
    """

    @abstractmethod
    async def __aenter__(self) -> AbstractFlashSession:
        """
        Create a flashing session.
        """
        raise NotImplementedError()

    async def __aexit__(self, ex_type, ex, tb):
        pass

    def __str__(self):
        return type(self).__name__
