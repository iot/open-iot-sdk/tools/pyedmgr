# Copyright (c) 2022, Arm Limited and Contributors. All rights reserved.
# SPDX-License-Identifier: Apache-2.0
"""
IrisFlasher.
        """

import asyncio
import logging
import time
from typing import Any, Callable, Coroutine, Union

import aiofiles

from iris import iris
import iris.debug

from .abstract import AbstractFlasher, AbstractFlashSession, FirmwareBlockContext
from ..internal import asyncify

try:
    from iris.debug.Target import SyncTarget as IrisTarget
except:
    from iris.debug.Target import Target as IrisTarget

class _IrisFlashSession(AbstractFlashSession):
    MAX_FAILED_WRITE_ATTEMPTS = 5

    def __init__(self, target: IrisTarget, logger: logging.Logger):
        super().__init__()
        self._target = target
        self._logger = logger
        self._have_flashed = False

    @property
    def preferred_block_size(self) -> int:
        return -1

    @property
    def _auto_load_blocks(self) -> bool:
        return False

    async def send_file_block(
        self,
        block: FirmwareBlockContext,
        start_time: int = None,
    ) -> Coroutine[Any, Any, Any]:
        """
        Send a block as returned from next(AbstractFlashSession)
        """
        # Load firmware as application on CPU 0.
        if block.firmware:
            if self._have_flashed:
                self._logger.warning(
                    f"Multiple firmware files specified, ignoring {block.filename}"
                )
                return
            start_time = time.time()
            self._target.load_application(block.firmware.path)
            elapsed_ms = 1000 * (time.time() - start_time)
            block.get_stats(elapsed_ms).log_to(self._logger)
            self._have_flashed = True
            return

        # Load files as data.
        if not block.stream:
            block.stream = await aiofiles.open(block.filename, "rb")
        block.block_bytes = await block.stream.read(block.block_size)

        # Write may fail under some unknown circumstances when multiples files are loaded.
        # It is attempted multiple time to work around the sporadic loading issue with the FVP.
        failed_write_attempts = 0
        while True:
            try:
                self._target.write_memory(
                    address=block.block_address,
                    data=bytearray(block.block_bytes),
                    size=1,
                    count=len(block.block_bytes),
                )
                break
            except Exception as e:
                failed_write_attempts = failed_write_attempts + 1
                if failed_write_attempts < self.MAX_FAILED_WRITE_ATTEMPTS:
                    self._logger.warning(
                        f"Failed to write {block.filename}, attempts lefts: {self.MAX_FAILED_WRITE_ATTEMPTS - failed_write_attempts}"
                    )
                    await asyncio.sleep(1 * failed_write_attempts)
                else:
                    raise e

    async def open(self):
        """
        Initialise session.
        """
        pass

    async def close(self):
        """
        Finalise session.
        """
        pass


class IrisFlasher(AbstractFlasher):
    """
    Flash an FVP using IRIS.
    """

    def __init__(self, target: IrisTarget, logger: logging.Logger = None):
        self._target = target
        self._logger = logger if logger else logging.getLogger(str(self))
        self._session = None

    async def __aenter__(self) -> AbstractFlashSession:
        """
        Create a flashing session.
        """
        self._session = _IrisFlashSession(self._target, self._logger)
        await self._session.open()
        return self._session

    async def __aexit__(self, ex_type, ex, tb):
        await self._session.close()
        self._session = None
