# Copyright (c) 2022, Arm Limited and Contributors. All rights reserved.
# SPDX-License-Identifier: Apache-2.0

"""
Null flash session.
        """

from typing import Any, Coroutine
from .abstract import (
    AbstractFlasher,
    AbstractFlashSession,
    FirmwareBlockContext,
    FirmwareList,
)

__all__ = ["NullFlasher"]


class _NullFlashSession(AbstractFlashSession):
    """
    NullFlasher session.
    """

    @property
    def firmware_list(self) -> FirmwareList:
        """
        Gets the list of files and firmware to be transferred. Files and firmware may be added by users.
        """
        return FirmwareList()

    async def send_file_block(
        self,
        block: FirmwareBlockContext,
        start_time: int = None,
    ) -> Coroutine[Any, Any, Any]:
        """
        Send a block as returned from next(AbstractFlashSession)
        """
        return None


class NullFlasher(AbstractFlasher):
    """
    Flasher which does nothing.
    """

    async def __aenter__(self) -> AbstractFlashSession:
        """
        Create a flashing session.
        """
        return _NullFlashSession()

    async def __aexit__(self, ex_type, ex, tb):
        pass
