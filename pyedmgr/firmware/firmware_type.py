# Copyright (c) 2022, Arm Limited and Contributors. All rights reserved.
# SPDX-License-Identifier: Apache-2.0

"""
Firmware type.
"""

from enum import IntFlag

# All ELF files start with \x7FELF; hex files don't have a magic number but they contain only the uppercase hex set
# plus 0x3a and 0x0a.
_ELF_MAGIC = b"\x7FELF"
_HEX_LEN = 64
_HEX_CHARS = {
    0x0A,
    0x30,
    0x31,
    0x32,
    0x33,
    0x34,
    0x35,
    0x36,
    0x37,
    0x38,
    0x39,
    0x3A,
    0x41,
    0x42,
    0x43,
    0x44,
    0x45,
    0x46,
}


class FirmwareType(IntFlag):
    """
    Firmware type.
    """

    @classmethod
    def of_file(cls, path):
        """
        Get firmware type from a file. Raise OSError if the file is not readable.
        """
        with open(path, "rb") as file:
            bs = file.read(len(_ELF_MAGIC))
            if bs == _ELF_MAGIC:
                return cls.ELF
            bs += file.read(_HEX_LEN - len(_ELF_MAGIC))
            if all(b in _HEX_CHARS for b in bs):
                return cls.HEX
            return cls.BINARY

    BINARY = 1 << 1
    ELF = 1 << 2
    HEX = 1 << 3
