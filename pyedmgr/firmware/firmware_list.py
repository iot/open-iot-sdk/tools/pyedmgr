# Copyright (c) 2022, Arm Limited and Contributors. All rights reserved.
# SPDX-License-Identifier: Apache-2.0

"""
Firmware list.
"""

from collections import UserList
from typing import Iterable, Iterator, SupportsIndex, Tuple, Union

from .firmware import Firmware


def _check_type(value: Union[Firmware, Tuple[str, int]]):
    if isinstance(value, tuple):
        if len(value) != 2:
            raise TypeError("If type(value) is tuple it must have two items")
        if not isinstance(value[0], str):
            raise TypeError("type(value[0]) must be str")
        if not isinstance(value[1], int):
            raise TypeError("type(value[1]) must be int")
    elif not isinstance(value, Firmware):
        raise TypeError(f"type(value) must be tuple or Firmware, not {type(value)}")


class FirmwareList(UserList):
    """
    List of firmware.
    """

    def __init__(
        self, initial_items: Iterable[Union[Firmware, Tuple[str, int]]] = None
    ):
        super().__init__()
        if initial_items:
            self.data = [None] * len(initial_items)
            for i, value in enumerate(initial_items):
                self[i] = value

    def __iter__(self) -> Iterator[Union[Firmware, Tuple[str, int]]]:
        return iter(self.data)

    def __setitem__(self, i: SupportsIndex, value: Union[Firmware, Tuple[str, int]]):
        _check_type(value)
        self.data[i] = value

    def append(self, item: Union[Firmware, Tuple[str, int]]):
        _check_type(item)
        self.data.append(item)

    def insert(self, i: SupportsIndex, item: Union[Firmware, Tuple[str, int]]):
        _check_type(item)
        self.data.insert(i, item)
