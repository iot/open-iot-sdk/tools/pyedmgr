# Copyright (c) 2022, Arm Limited and Contributors. All rights reserved.
# SPDX-License-Identifier: Apache-2.0
"""
Firmware.
        """

from .firmware_type import FirmwareType


class Firmware:
    """
    Firmware abstraction.
    """

    def __init__(self, path: str):
        """
        Initialise firmware.
        :param path: Names a binary file which contains firmware to flash. May be an ELF file or flat binary depending
        on target.
        """
        self._path = path
        self._type = FirmwareType.of_file(path)

    @property
    def path(self):
        """
        Gets the path to the file.
        """
        return self._path

    @property
    def type(self) -> FirmwareType:
        """
        Indicates the Firmware type.
        """
        return self._type

    def __str__(self):
        return f"[{self._type.name}] {self._path}"
