# Copyright (c) 2022, Arm Limited and Contributors. All rights reserved.
# SPDX-License-Identifier: Apache-2.0
"""
Tests for pyedmgr.internal.util.
        """

# pylint:disable=missing-class-docstring
# pylint:disable=missing-function-docstring
# pylint:disable=redefined-outer-name
# pylint:disable=invalid-name
# pylint:disable=disallowed-name

import asyncio
import logging
import pytest

import nest_asyncio

from pyedmgr.internal.util import (
    Expect,
    LogStream,
    dict_rename,
    dict_remove,
    asyncify,
    syncify,
)


class Test_Expect:
    def test__has_type__should_pass_when_type_is_correct(self):
        Expect.has_type(0, int, "")

    def test__has_type__should_raise_TypeError_when_type_is_wrong(self):
        with pytest.raises(TypeError):
            Expect.has_type(0, str, "")

    def test__has_key__should_pass_when_key_is_found(self):
        Expect.has_key({"a": 1}, "a", "")

    def test__has_key__should_raise_KeyError_when_key_is_missing(self):
        with pytest.raises(KeyError):
            Expect.has_key({"a": 1}, "b", "")

    def test__has_key_of_type__should_pass_when_key_is_found_and_type_is_correct(self):
        Expect.has_key_of_type({"a": 1}, "a", int, "")

    def test__has_key_of_type__should_raise_KeyError_when_key_is_missing(self):
        with pytest.raises(KeyError):
            Expect.has_key_of_type({"a": 1}, "b", int, "")

    def test__has_key_of_type__should_raise_KeyError_when_key_is_found_but_type_is_wrong(
        self,
    ):
        with pytest.raises(TypeError):
            Expect.has_key_of_type({"a": 1}, "a", str, "")

    def test__is_tuple_of__should_pass_when_types_are_correct(self):
        Expect.is_tuple_of(("a", 1, 0.5), (str, int, float), "")

    def test__is_tuple_of__should_raise_ValueError_when_len_is_wrong(self):
        with pytest.raises(ValueError):
            Expect.is_tuple_of(("a", 1, 0.5), (str, int), "")

    def test__is_tuple_of__should_raise_TypeError_when_type_is_wrong(self):
        with pytest.raises(TypeError):
            Expect.is_tuple_of(("a", 1, 0.5), (int, int, float), "")


class MockLogger:
    def __init__(self):
        self.invocations = []

    def log(self, level, line):
        self.invocations.append((level, line))


class Test_LogStream:
    def test__should_log_on_eol(self):
        logger = MockLogger()
        stream = LogStream(logging.DEBUG, logger)
        assert len(logger.invocations) == 0

        stream.write("abc")
        assert len(logger.invocations) == 0

        stream.write("\n")
        assert len(logger.invocations) == 1
        assert logger.invocations[0] == (logging.DEBUG, "abc")

    def test__should_log_on_stream_destroyed(self):
        logger = MockLogger()
        stream = LogStream(logging.DEBUG, logger)
        assert len(logger.invocations) == 0

        stream.write("abc")
        assert len(logger.invocations) == 0

        del stream
        assert len(logger.invocations) == 1
        assert logger.invocations[0] == (logging.DEBUG, "abc")

    def test__should_allow_change_level(self):
        logger = MockLogger()
        stream = LogStream(logging.DEBUG, logger)
        assert len(logger.invocations) == 0

        stream.write("abc\n")
        assert len(logger.invocations) == 1
        assert logger.invocations[-1] == (logging.DEBUG, "abc")

        stream.level = logging.ERROR
        stream.write("def\n")
        assert len(logger.invocations) == 2
        assert logger.invocations[-1] == (logging.ERROR, "def")


class Test_dict_rename:
    def test__should_rename_key(self):
        d = {"a": 1}
        return_value = dict_rename(d, "a", "b")
        assert return_value is True
        assert "a" not in d
        assert d["b"] == 1

    def test__should_do_nothing_if_key_not_found(self):
        d = {"a": 1}
        return_value = dict_rename(d, "b", "c")
        assert return_value is False
        assert d["a"] == 1


class Test_dict_remove:
    def test__should_remove_key(self):
        d = {"a": 1}
        return_value = dict_remove(d, "a")
        assert return_value is True
        assert "a" not in d
        assert len(d) == 0

    def test__should_do_nothing_if_key_not_found(self):
        d = {"a": 1}
        return_value = dict_remove(d, "b")
        assert return_value is False
        assert d["a"] == 1


class Test_asyncify:
    @pytest.mark.asyncio
    async def test__should_pass(self):
        x = 0

        def set_x(y=1):
            nonlocal x
            x = y

        await asyncify(set_x, 2)
        assert x == 2


class Test_syncify:
    def test__should_pass_with_no_event_loop(self):
        async def foo():
            await asyncio.sleep(0)
            return 123

        assert syncify(foo) == 123

    @pytest.mark.asyncio
    async def test__should_pass_with_running_event_loop(self):
        async def foo():
            await asyncio.sleep(0)
            return 123

        assert syncify(foo) == 123

    @pytest.mark.asyncio
    async def test__should_pass_with_nested_event_loop(self):
        nest_asyncio.apply()

        async def foo():
            await asyncio.sleep(0)
            return 123

        assert syncify(foo) == 123
