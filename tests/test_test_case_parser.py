# Copyright (c) 2022, Arm Limited and Contributors. All rights reserved.
# SPDX-License-Identifier: Apache-2.0
"""
Tests for fixture_test_case.
        """

# pylint:disable=missing-function-docstring
# pylint:disable=redefined-outer-name

import pytest

from pyedmgr import TestCaseParser


@pytest.mark.parametrize(
    "test_case_descriptor,expected_result",
    [
        (
            # Input and output should be the same
            {
                "FVP_Corstone_SSE-300_Ethos-U55": {
                    "firmware": ["firmware.bin", ("file.bin", 0x1000)],
                    "config": "MPS3.conf",
                    "args": ["-Q", "100"],
                }
            },
            [
                [
                    (
                        "FVP_Corstone_SSE-300_Ethos-U55",
                        {
                            "firmware": ["firmware.bin", ("file.bin", 0x1000)],
                            "config": "MPS3.conf",
                            "args": ["-Q", "100"],
                        },
                    )
                ],
            ],
        ),
        (
            # A firmware list should be mapped to dictionary key 'firmware'.
            {"FVP_Corstone_SSE-300_Ethos-U55": ["firmware.bin", ("file.bin", 0x1000)]},
            [
                [
                    (
                        "FVP_Corstone_SSE-300_Ethos-U55",
                        {"firmware": ["firmware.bin", ("file.bin", 0x1000)]},
                    )
                ],
            ],
        ),
        (
            # Should be able to specify devices multiple times, with the dictionary format and the firmware list format.
            {
                "FVP_Corstone_SSE-300_Ethos-U55": {
                    1: {
                        "firmware": ["firmware.bin", ("file.bin", 0x1000)],
                        "config": "MPS3.conf",
                        "args": ["-Q", "100"],
                    },
                    2: ["firmware-2.bin", ("file-2.bin", 0x2000)],
                }
            },
            [
                [
                    (
                        "FVP_Corstone_SSE-300_Ethos-U55",
                        {
                            "firmware": ["firmware.bin", ("file.bin", 0x1000)],
                            "config": "MPS3.conf",
                            "args": ["-Q", "100"],
                        },
                    ),
                    (
                        "FVP_Corstone_SSE-300_Ethos-U55",
                        {"firmware": ["firmware-2.bin", ("file-2.bin", 0x2000)]},
                    ),
                ],
            ],
        ),
        (
            # Should be able to parse embedded JSON.
            '{"FVP_Corstone_SSE-300_Ethos-U55": ["firmware.bin", ["file.bin",4096]]}',
            [
                [
                    (
                        "FVP_Corstone_SSE-300_Ethos-U55",
                        {"firmware": ["firmware.bin", ["file.bin", 0x1000]]},
                    ),
                ],
            ],
        ),
        (
            # Should be able to parse JSON from a file.
            "@test_test_case_parser.json",
            [
                [
                    (
                        "FVP_Corstone_SSE-300_Ethos-U55",
                        {
                            "firmware": ["firmware.bin", ["file.bin", 0x1000]],
                            "config": "MPS3.conf",
                            "args": ["-Q", "100"],
                        },
                    ),
                    (
                        "FVP_Corstone_SSE-300_Ethos-U55",
                        {"firmware": ["firmware-2.bin", ["file-2.bin", 0x2000]]},
                    ),
                ],
            ],
        ),
        (
            # Should handle multiple test cases
            [
                {
                    "FVP_Corstone_SSE-300_Ethos-U55": {
                        "firmware": ["firmware.bin", ("file.bin", 0x1000)]
                    }
                },
                {
                    "FVP_Corstone_SSE-300_Ethos-U55": [
                        "firmware-2.bin",
                        ("file-2.bin", 0x2000),
                    ]
                },
            ],
            [
                [
                    (
                        "FVP_Corstone_SSE-300_Ethos-U55",
                        {"firmware": ["firmware.bin", ("file.bin", 0x1000)]},
                    )
                ],
                [
                    (
                        "FVP_Corstone_SSE-300_Ethos-U55",
                        {"firmware": ["firmware-2.bin", ("file-2.bin", 0x2000)]},
                    )
                ],
            ],
        ),
    ],
)
def test__should_parse_test_cases(test_case_descriptor, expected_result):
    test_cases = TestCaseParser(test_case_descriptor).parse()
    actual_result = list(map(lambda tc: tc._device_firmware_mapping, test_cases))
    assert actual_result == expected_result
