# Copyright (c) 2022, Arm Limited and Contributors. All rights reserved.
# SPDX-License-Identifier: Apache-2.0
"""
Tests for fixture_test_case.
        """

# pylint:disable=missing-function-docstring
# pylint:disable=redefined-outer-name

import pytest

from pyedmgr import Firmware, TestCase
from pyedmgr.internal import envify


@pytest.mark.asyncio
@pytest.mark.parametrize(
    "test_case_descriptor,expected_error",
    [
        (
            {
                "FVP_Corstone_SSE-300_Ethos-U55": {
                    "firmware": ["{{PWD}}/cs300-u55.elf"],
                    "config": "MPS3.conf",
                },
            },
            None,
        ),
        (
            {
                "FVP_Corstone_SSE-300_Ethos-U55": {
                    "firmware": [
                        "{{PWD}}/cs300-u55.elf",
                        ("{{PWD}}/cs300-u55.elf", 0x1000),
                    ],
                    "config": "MPS3.conf",
                },
            },
            None,
        ),
        (
            {
                "FVP_Corstone_SSE-300_Ethos-U55": {
                    "firmware": ["{{PWD}}/cs300-u55.elf"],
                    "config": "MPS3.conf",
                },
                "FVP_Corstone_SSE-300_Ethos-U65": {
                    "firmware": ["{{PWD}}/cs300-u55.elf"],
                    "config": "MPS3.conf",
                },
            },
            None,
        ),
        (
            {
                "FVP_Corstone_SSE-300_Ethos-U55": {
                    "firmware": ["{{PWD}}/cs300-u55.elf"],
                    "config": "MPS3.conf",
                },
                "FVP_Corstone_SSE-300_Ethos-U65": {
                    "firmware": [
                        "{{PWD}}/cs300-u55.elf",
                        ("{{PWD}}/cs300-u55.elf", 0x1000),
                    ],
                    "config": "MPS3.conf",
                },
            },
            None,
        ),
        (
            {
                "FVP_Corstone_SSE-300_Ethos-U55": {
                    "firmware": [
                        "{{PWD}}/cs300-u55.elf",
                        ("{{PWD}}/cs300-u55.elf", 0x1000),
                    ],
                    "config": "MPS3.conf",
                },
                "FVP_Corstone_SSE-300_Ethos-U65": {
                    "firmware": [
                        "{{PWD}}/cs300-u55.elf",
                        ("{{PWD}}/cs300-u55.elf", 0x1000),
                    ],
                    "config": "MPS3.conf",
                },
            },
            None,
        ),
        (
            {
                "FVP_Corstone_SSE-300_Ethos-U55": {
                    "firmware": ["{{PWD}}/unavailable.elf"],
                    "config": "MPS3.conf",
                },
            },
            FileNotFoundError,
        ),
        (
            {
                "blah": {"firmware": ["{{PWD}}/cs300-u55.elf"], "config": "MPS3.conf"},
            },
            KeyError,
        ),
    ],
)
async def test_test_case(test_case_descriptor, expected_error):
    async def do_test():
        async with TestCase.parse(test_case_descriptor)[0] as context:
            for device_name, device_args in context.device_firmware_mapping:
                # pylint:disable=cell-var-from-loop
                device = list(
                    filter(lambda d: device_name in d.name, context.allocated_devices)
                )
                firmware = []
                try:
                    firmware = list(
                        filter(
                            lambda f: isinstance(f, (Firmware, str)),
                            device_args["firmware"],
                        )
                    )
                except KeyError:
                    assert False  # KeyError is sometimes expected, but not when accessing the firmware list
                # pylint:enable=cell-var-from-loop
                assert (
                    envify(firmware[0]) == device[0].firmware.path
                )  # The firmware was flashed to the device

    if bool(expected_error):
        with pytest.raises(expected_error):
            await do_test()
    else:
        await do_test()
