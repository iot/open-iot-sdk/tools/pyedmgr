# Copyright (c) 2022, Arm Limited and Contributors. All rights reserved.
# SPDX-License-Identifier: Apache-2.0
"""
Tests for DeviceFactory, Device, SerialChannel/ServerController and SocketChannel/ServerController.
        """

# pylint:disable=missing-class-docstring
# pylint:disable=missing-function-docstring
# pylint:disable=redefined-outer-name
# pylint:disable=invalid-name
# pylint:disable=disallowed-name

import pytest

from pyedmgr import (
    DeviceFactory,
    SerialChannel,
    SerialServerController,
    SocketChannel,
    SocketServerController,
)

from pyedmgr.internal.const import (
    DEFAULT_BAUD,
    DEFAULT_HOST,
    DEFAULT_PORT,
    DEFAULT_TIMEOUT,
)


class Test_DeviceFactory:
    @pytest.mark.asyncio
    @pytest.mark.parametrize("baud,timeout", [(9600, 1), (None, None)])
    async def test__should_create_serial_device_with_config(self, baud, timeout):
        device = DeviceFactory().fake_serial(baud=baud, timeout=timeout)
        await device.connect()
        try:
            assert isinstance(device.channel, SerialChannel)
            assert isinstance(device.controller, SerialServerController)
            assert device.channel._baud == (baud if baud else DEFAULT_BAUD)
            assert device.channel._timeout == (timeout if timeout else DEFAULT_TIMEOUT)
            assert device.controller._baud == (baud if baud else DEFAULT_BAUD)
        finally:
            await device.disconnect(shutdown=True)

    @pytest.mark.asyncio
    @pytest.mark.parametrize("host,port", [("localhost", 1234), (None, None)])
    async def test__should_create_socket_device_with_config(self, host, port):
        device = DeviceFactory().fake_socket(host=host, port=port)
        await device.connect()
        try:
            assert isinstance(device.channel, SocketChannel)
            assert isinstance(device.controller, SocketServerController)
            assert device.channel._host == (host if host else DEFAULT_HOST)
            assert device.channel._port == (port if port else DEFAULT_PORT)
            assert device.controller._host == (host if host else DEFAULT_HOST)
            assert device.controller._port == (port if port else DEFAULT_PORT)
        finally:
            await device.disconnect(shutdown=True)


class Test_Device:
    @pytest.mark.asyncio
    async def test__should_connect_and_disconnect(self):
        device = DeviceFactory().fake_socket()
        await device.connect()
        try:
            assert device.channel is not None
        finally:
            await device.disconnect()
            assert device.channel is None

    @pytest.mark.asyncio
    @pytest.mark.parametrize("bs", [b"hello world\r\n"])
    async def test__should_communicate_via_serial_interface(self, bs):
        device = DeviceFactory().fake_serial()
        await device.connect()
        try:
            await device.channel.write_async(bs)
            out = await device.channel.readline_async()
            assert bs == out
        finally:
            await device.disconnect(shutdown=True)

    @pytest.mark.asyncio
    @pytest.mark.parametrize("bs", [b"hello world\r\n"])
    async def test__should_communicate_via_socket_interface(self, bs):
        device = DeviceFactory().fake_socket()
        await device.connect()
        try:
            await device.channel.write_async(bs)
            out = await device.channel.readline_async()
            assert bs == out
        finally:
            await device.disconnect(shutdown=True)
