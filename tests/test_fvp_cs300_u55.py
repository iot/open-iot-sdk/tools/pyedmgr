# Copyright (c) 2022, Arm Limited and Contributors. All rights reserved.
# SPDX-License-Identifier: Apache-2.0

"""
Tests for fixture_test_case.
"""

# pylint:disable=missing-function-docstring
# pylint:disable=redefined-outer-name

import pytest

from pyedmgr import TestDevice, test_case_context, logger, TestCaseContext


@pytest.mark.parametrize(
    'test_case_context',
    [
        {
            "FVP_Corstone_SSE-300_Ethos-U55": {
                "firmware": ["{{PWD}}/cs300-u55.elf"],
                "config": "MPS3.conf",
                "args": ["-Q", "10"],
            }
        }
    ],
    indirect=['test_case_context']
)
@pytest.mark.asyncio
async def test_fvp_cs300_u55(test_case_context:TestCaseContext, logger):
    device:TestDevice = test_case_context.allocated_devices[0]
    expectations = [
        # In   Out
        (None, 'hello, world')
    ]

    expectations_met = await device.communicate(expectations, logger=logger)

    assert expectations_met == len(expectations)
