# v2023.07 (2023-07-18)

## Changes

* docs: Add user documentation & API reference
* test: Add test_case_context fixture
* ci: Fix name of script
* ci: Add missing tag to pages deployment
* fvp: Handle return value of shutil.which when FVP binary not found
* io: Do not crash when decoding invalid UTF-8 codepoints
* fvp: Update Target class name for newer FVP Iris.
* Iris: Retry block write in case of failure
* fvp: Get hostname from IrisTcpClient object for compatibility with newer FVP Iris.
* ci: Update developer-tools.


# v2023.04 (2023-04-13)

## Changes

* ci: Enable possible tpip violation warning messages in Merge Requests.


# v2023.01 (2023-01-19)

## Changes

* ci: Enable autobot and update developer-tools


This changelog should be read in conjunction with release notes provided
for a specific release version.
