# Copyright (c) 2022, Arm Limited and Contributors. All rights reserved.
# SPDX-License-Identifier: Apache-2.0
import setuptools

setuptools.setup(
    name="pyedmgr",
    version="0.0.1",
    author="Chris Swinchatt",
    author_email="christopher.swinchatt@arm.com",
    description="Python Embedded Device Manager",
    # url="",
    # project_urls={},
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: Apache 2.0",
        "Operating System :: Linux",
    ],
    packages=[
        "pyedmgr",
        "pyedmgr.channel",
        "pyedmgr.controller",
        "pyedmgr.device",
        "pyedmgr.firmware",
        "pyedmgr.flasher",
        "pyedmgr.internal",
        "pyedmgr.registry",
        "pyedmgr.test",
    ],
    python_requires=">=3.6",
)
