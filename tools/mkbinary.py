#!/usr/bin/env python3

# Copyright (c) 2022, Arm Limited and Contributors. All rights reserved.
# SPDX-License-Identifier: Apache-2.0

"""
Usage: {0} [binary file [string]]

Overwrite symbol in [binary file] with [string].

If [string] is not given, it wil be read from standard input.
        """

import os
import re
from subprocess import Popen, PIPE, STDOUT
import sys

from Hellf import ELF

NAME = os.path.basename(sys.argv[0])
ELFIMG = "firmware.elf"
SECTION = ".data"
START_SYM = "__printer_string_start__"
END_SYM = "__printer_string_end__"
STRING = None


def show_usage(stream=None):
    stream = stream if stream else sys.stdout
    print(__doc__.format(sys.argv[0]))


def die_usage(status):
    stream = sys.stdout
    if status:
        stream = sys.stderr
    show_usage(stream)
    exit(status)


def invoke(*args):
    with Popen(args, stdout=PIPE, stderr=STDOUT) as p:
        stdout, _ = p.communicate()
        return p.returncode, stdout.decode("utf-8". "replace")


def invoke2(*args):
    res, stdout = invoke(*args)
    if res:
        print(stdout, file=sys.stderr)
        exit(res)
    return stdout


def get_vaddr(match, *args):
    stdout = invoke2(*args)
    for line in stdout.splitlines():
        if not line:
            continue
        if re.search(match, line):
            m = re.search(r"[0-9a-fA-F]{8}", line)
            vaddr = int("0x" + m.group(0), 16)
            print(f" > Found {match} at 0x{vaddr:x}")
            return vaddr
    print(f"{NAME}: Did not find {match}")
    exit(1)


if __name__ == "__main__":
    if any(("-h" in sys.argv, "--help" in sys.argv)):
        die_usage(0)

    if len(sys.argv) >= 2:
        ELFIMG = sys.argv[1]
    if len(sys.argv) >= 3:
        STRING = sys.argv[2]

    if not os.path.isfile(ELFIMG):
        print(f"{NAME}: {ELFIMG}: No such file or directory")
        die_usage(1)

    patched = re.sub(r".(.+)$", r".patched.\1", ELFIMG)
    binary = re.sub(r"..+$", ".bin", ELFIMG)

    # Find offset of START_SYM relative to SECTION and space between START_SYM and END_SYM.
    print(f"> Reading {ELFIMG}...")
    section_vaddr = get_vaddr(SECTION, "arm-none-eabi-readelf", "-S", ELFIMG)
    symbol_vaddr = get_vaddr(START_SYM, "arm-none-eabi-objdump", "-t", ELFIMG)
    section_size = (
        get_vaddr(END_SYM, "arm-none-eabi-objdump", "-t", ELFIMG) - symbol_vaddr - 1
    )  # Leave NUL terminator
    offset = symbol_vaddr - section_vaddr

    string_bytes = b""
    use_prompt = os.isatty(0)
    while True:
        prompt = ""
        if use_prompt and not STRING:
            prompt = f"String to write (up to {section_size-1} bytes): "
            STRING = input(prompt)
        elif not STRING:
            STRING = sys.stdin.read()
        if not STRING.endswith(
            "\n"
        ):  # input() trims \n but STRING could come from command-line on our first iteration
            STRING += "\n"
        string_bytes = STRING.encode("ascii")
        if len(string_bytes) <= section_size:
            break
        print(f"{NAME}: Input string is too long.")
        STRING = None
        use_prompt = True
    # Pad with NUL.
    if len(string_bytes) < section_size:
        string_bytes += b"\0" * (section_size - len(string_bytes))

    # Patch ELF.
    print(
        f"> Creating patched ELF {patched} with input string copied into section {SECTION} at bytes {offset} to {offset + len(string_bytes)}"
    )
    elf = ELF(ELFIMG)
    section_bytes = list(elf.get_section_by_name(SECTION).data)
    for i, b in enumerate(string_bytes):
        section_bytes[offset + i] = b
    elf.get_section_by_name(SECTION).data = bytes(section_bytes)
    elf.save(patched)

    # Convert to binary
    print(f"> Creating flat binary {binary}")
    invoke2("arm-none-eabi-objcopy", "-O", "binary", patched, binary)

    print("Done")
